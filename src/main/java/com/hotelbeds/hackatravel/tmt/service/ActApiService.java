package com.hotelbeds.hackatravel.tmt.service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.xml.bind.DatatypeConverter;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.hotelbeds.tab.sdk.model.domain.ActivityOrderType;
import com.hotelbeds.tab.sdk.model.domain.ActivityType;
import com.hotelbeds.tab.sdk.model.pojo.Activity;
import com.hotelbeds.tab.sdk.model.pojo.ActivitySearchFilterItem;
import com.hotelbeds.tab.sdk.model.pojo.ActivitySearchFilterItemList;
import com.hotelbeds.tab.sdk.model.pojo.PaginationRequest;
import com.hotelbeds.tab.sdk.model.pojo.Segment;
import com.hotelbeds.tab.sdk.model.request.ActivitySearchRequest;
import com.hotelbeds.tab.sdk.model.response.ActivitySearchResponse;
import com.hotelbeds.tab.sdk.model.response.SegmentsResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ActApiService {
    private String apiKey = "qd2jrx5k99cwynrqj2qge742";
    private String secret = "Bj2fds97mj";
    private String xSignature = "3658c799301a8d203ac91e648066b610b3934110a1936713fc3a34a6023170e3";
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
    private Object segments;
    private List<Segment> cac;

    @PostConstruct
    public void init() {
        Map<String, String> map = new HashMap<>();
        map.put("From", "2018-05-01");
        map.put("To", "2018-05-04");
        getXSignature();
        //getItemsFiltered(map, new ActivitySearchFilterItem(ActivityFilterType.PRICETO, "30"));
    }

    private String getXSignature() {
        int floorDate = (int) Math.floor((double) new Date().getTime() / (double) 1000);
        String assemble = apiKey + secret + floorDate;
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(assemble.getBytes(StandardCharsets.UTF_8));
            return DatatypeConverter.printHexBinary(hash).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public MultiValueMap<String, String> getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Api-key", apiKey);
        String xSignature = getXSignature();
        headers.add("X-Signature", xSignature == null ? this.xSignature : xSignature);
        headers.add("Content-Type", "application/json");
        return headers;
    }

    public <I> I getItems(HttpMethod httpMethod, String body, Class<I> item, String url) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = new HttpEntity<>(body, getHeaders());
        return restTemplate.exchange(url, httpMethod, entity, item).getBody();
    }

    private List<Activity> activitiesCached = new ArrayList<>();

    public List<Activity> getItemsFiltered(Map<String, String> mapa, List<ActivitySearchFilterItem> activitySearchFilterItems) {
        ObjectMapper mapper = new ObjectMapper();
        List<Activity> activities = new ArrayList<>();
        try {
            ActivitySearchFilterItemList lastAction = new ActivitySearchFilterItemList();
            for (ActivitySearchFilterItem activitySearchFilterItem : activitySearchFilterItems) {
                List<ActivitySearchFilterItem> searchFilterItems = lastAction.getSearchFilterItems();
                if (searchFilterItems == null) {
                    searchFilterItems = new ArrayList<>();
                }
                searchFilterItems.add(activitySearchFilterItem);
                lastAction.setSearchFilterItems(searchFilterItems);
            }
            ActivitySearchFilterItemList activitySearchFilterItemList = new ActivitySearchFilterItemList();
            activitySearchFilterItemList.setSearchFilterItems(lastAction.getSearchFilterItems());
            ActivitySearchRequest activitySearchRequest = new ActivitySearchRequest();
            activitySearchRequest.getFilters().add(activitySearchFilterItemList);
            activitySearchRequest.setFrom(getFrom(mapa));
            activitySearchRequest.setTo(getTo(mapa));
            activitySearchRequest.setLanguage("en");
            PaginationRequest paginationRequest = new PaginationRequest();
            paginationRequest.setItemsPerPage(10);
            paginationRequest.setPage(1);
            activitySearchRequest.setOrder(ActivityOrderType.DEFAULT);
            activitySearchRequest.setPagination(paginationRequest);
            try{
                List<Activity> activities1 = getItems(HttpMethod.POST, mapper.writeValueAsString(activitySearchRequest), ActivitySearchResponse.class,
                    "https://api.test.hotelbeds.com/activity-api/3.0/activities").getActivities();
                if (activities1 != null && !activities1.isEmpty()) {
                    if (activitiesCached.size() < activities1.size()) {
                        activitiesCached.addAll(activities1);
                        List<Segment> segments = getSegments();
                        List<Activity> newIntegrators = new ArrayList<>();
                        for(Segment segment : segments){
                            for(Activity activity : activitiesCached){
                                Random random1 = new Random();
                                activity.setPaxes(random1.nextInt(15) + 5);
                                activity.setFreeSlots(random1.nextInt(activity.getPaxes()));
                                newIntegrators.add(activity.createNew(segment));
                            }
                        }
                        activitiesCached.addAll(newIntegrators);
                    }
                }else{
                    return activitiesCached;
                }
                return activities1;
            }catch(Exception e){
                return activitiesCached;
            }


        } catch (Exception e) {
            System.out.println(e);
        }
        Activity activity = new Activity();
        activity.setCode("1");
        activity.setContentId("c");
        activity.setCurrency("EU");
        activity.setCurrencyName("EUR");
        activity.setName("actividad1");
        activity.setType(ActivityType.TICKET);
        activities.add(activity);
        return activities;
    }

    private Date getFrom(Map<String, String> mapa) {
        return getDate(mapa, "From");
    }

    private Date getTo(Map<String, String> mapa) {
        return getDate(mapa, "To");
    }

    private Date getDate(Map<String, String> mapa, String item) {
        String dateString = mapa.get(item);
        try {
            return SDF.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<Segment> cacheSegment;

    public List<Segment> getSegments() {
        try {
            if (cacheSegment == null) {
                cacheSegment = getItems(HttpMethod.GET, "", SegmentsResponse.class,
                    "https://api.test.hotelbeds.com/activity-content-api/3.0/segments/en").getSegmentationGroups().stream().flatMap(
                    segmentGroup -> segmentGroup.getSegments().stream()).filter(s -> s.getName().length() > 15).skip(2)
                    .collect(Collectors.toList());
            }
            return cacheSegment;
        } catch (Exception e) {
            e.printStackTrace();
            List<Segment> segments = new ArrayList<>();
            segments.add(new Segment(1, "Activities", "Icon1", null));
            segments.add(new Segment(2, "Nature", "Icon2", null));
            return segments;
        }
    }

    public List<Activity> getActivities() {
        return activitiesCached;
    }
}
