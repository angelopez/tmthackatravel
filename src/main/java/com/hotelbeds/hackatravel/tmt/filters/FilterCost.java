package com.hotelbeds.hackatravel.tmt.filters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelbeds.hackatravel.tmt.service.ActApiService;
import com.hotelbeds.tab.sdk.model.domain.ActivityFilterType;
import com.hotelbeds.tab.sdk.model.pojo.Activity;
import com.hotelbeds.tab.sdk.model.pojo.ActivitySearchFilterItem;


@Service
public class FilterCost implements Filter {
    @Autowired
    private ActApiService actApiService;

    @Override
    public List<Activity> retrieveWithFilter(
        Map<String, String> mapa,
        List<ActivitySearchFilterItem> preFilters) {
        return actApiService.getItemsFiltered(mapa, getFilters(mapa,preFilters));
    }

    @Override
    public List<ActivitySearchFilterItem> getFilters(
        Map<String, String> mapa,
        List<ActivitySearchFilterItem> preFilters) {
        String currentFilter = mapa.getOrDefault("Content", mapa.getOrDefault("content","2"));
        int price = Integer.parseInt(currentFilter);
        List<ActivitySearchFilterItem> toReturn = new ArrayList<>();

        if (price == 1) {
            toReturn.add(new ActivitySearchFilterItem(ActivityFilterType.PRICETO, "30"));
            toReturn.add(new ActivitySearchFilterItem(ActivityFilterType.PRICEFROM, "0"));
        } else if (price == 2) {
            toReturn.add(new ActivitySearchFilterItem(ActivityFilterType.PRICETO, "100"));
            toReturn.add(new ActivitySearchFilterItem(ActivityFilterType.PRICEFROM, "30"));
        } else {
            toReturn.add(new ActivitySearchFilterItem(ActivityFilterType.PRICETO, "999999"));
            toReturn.add(new ActivitySearchFilterItem(ActivityFilterType.PRICEFROM, "100"));
        }
        toReturn.addAll(preFilters);
        return toReturn;
    }
}
