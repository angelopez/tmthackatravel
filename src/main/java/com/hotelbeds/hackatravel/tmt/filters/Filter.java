package com.hotelbeds.hackatravel.tmt.filters;

import java.util.List;
import java.util.Map;

import com.hotelbeds.tab.sdk.model.pojo.Activity;
import com.hotelbeds.tab.sdk.model.pojo.ActivitySearchFilterItem;

public interface Filter {
    List<Activity> retrieveWithFilter(Map<String, String> mapa, List<ActivitySearchFilterItem> preFilters);
    List<ActivitySearchFilterItem> getFilters(
        Map<String, String> mapa,
        List<ActivitySearchFilterItem> preFilters);
}
