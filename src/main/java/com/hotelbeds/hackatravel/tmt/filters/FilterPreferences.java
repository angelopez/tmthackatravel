package com.hotelbeds.hackatravel.tmt.filters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelbeds.hackatravel.tmt.controllers.SegmentController;
import com.hotelbeds.hackatravel.tmt.service.ActApiService;
import com.hotelbeds.tab.sdk.model.domain.ActivityFilterType;
import com.hotelbeds.tab.sdk.model.pojo.Activity;
import com.hotelbeds.tab.sdk.model.pojo.ActivitySearchFilterItem;
import com.hotelbeds.tab.sdk.model.pojo.Segment;

@Service
public class FilterPreferences implements Filter {
    @Autowired
    private ActApiService actApiService;
    @Autowired
    private SegmentController segmentController;

    @Override
    public List<Activity> retrieveWithFilter(
        Map<String, String> map,
        List<ActivitySearchFilterItem> preFilters) {
        return actApiService.getItemsFiltered(map, getFilters(map, preFilters));
    }

    @Override
    public List<ActivitySearchFilterItem> getFilters(
        Map<String, String> map,
        List<ActivitySearchFilterItem> preFilters) {
        List<Segment> segments = segmentController.getSegments();

        String cnt = map.getOrDefault("Content", map.getOrDefault("content", "Nature"));
        String[] segmentsRequested = cnt.split(",");
        List<ActivitySearchFilterItem> items = new ArrayList<>();
        int count = 0;
        for (String segmentRequested : segmentsRequested) {
            if (segmentRequested != null && !segmentRequested.isEmpty()
                && segments.stream().map(Segment::getName).collect(Collectors.toSet()).contains(segmentRequested)) {
                items.add(new ActivitySearchFilterItem(ActivityFilterType.SEGMENT, segmentRequested));
            }
            count++;
        }
        if (count >= segments.size()) {
            return new ArrayList<>();
        }
        items.addAll(preFilters);
        return items;
    }
}
