/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelbeds.hackatravel.tmt.filters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelbeds.hackatravel.tmt.service.ActApiService;
import com.hotelbeds.tab.sdk.model.domain.ActivityFilterType;
import com.hotelbeds.tab.sdk.model.pojo.Activity;
import com.hotelbeds.tab.sdk.model.pojo.ActivitySearchFilterItem;

@Service
public class FilterGroups implements Filter {
    @Autowired
    private ActApiService actApiService;

    @Override
    public List<Activity> retrieveWithFilter(
        Map<String, String> map,
        List<ActivitySearchFilterItem> preFilters) {
        return actApiService.getItemsFiltered(map, getFilters(map, preFilters));
    }

    @Override
    public List<ActivitySearchFilterItem> getFilters(
        Map<String, String> map,
        List<ActivitySearchFilterItem> preFilters) {
        String val = map.getOrDefault("Content", map.getOrDefault("content", "Alone"));
        List<ActivitySearchFilterItem> toReturn = new ArrayList<>();
        switch (val) {
            case "Alone":
                //toReturn.add(new ActivitySearchFilterItem(ActivityFilterType.SERVICE, "30"));
                break;
            case "Group":
                //toReturn.add(new ActivitySearchFilterItem(ActivityFilterType.SERVICE, "100"));
                break;
            default:
                //toReturn.add(new ActivitySearchFilterItem(ActivityFilterType.SERVICE, "100"));

        }
        toReturn.addAll(preFilters);
        return toReturn;
    }
}
