package com.hotelbeds.hackatravel.tmt.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hotelbeds.hackatravel.tmt.filters.FilterCost;
import com.hotelbeds.tab.sdk.model.pojo.Activity;

@RestController
@RequestMapping("/cost")
public class CostController {
    @Autowired
    private FilterController filterController;

    @RequestMapping(path = "")
    public List<Activity> List() {


        Map<String, String> mapa = new HashMap<>();
        Random number = new Random();

        int coste = number.nextInt((150 - 0) + 1) + 0;
        mapa.put("Select","Cost");
        mapa.put("content", Integer.toString(coste));
        return filterController.getFilter(mapa);
    }
}
