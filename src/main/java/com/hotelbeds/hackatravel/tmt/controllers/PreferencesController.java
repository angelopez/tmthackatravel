
package com.hotelbeds.hackatravel.tmt.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hotelbeds.hackatravel.tmt.filters.FilterPreferences;
import com.hotelbeds.tab.sdk.model.pojo.Activity;

@RestController
@RequestMapping("/preferences")
public class PreferencesController {
    @Autowired
    private FilterController filterController;

    @RequestMapping(path = "")
    public List<Activity> List() {
        Map<String, String> map = new HashMap();
        map.put("Select", "Activity");
        filterController.getFilter(map);
        return filterController.getFilter(map);
    }
}
