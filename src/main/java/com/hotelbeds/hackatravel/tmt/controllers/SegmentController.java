package com.hotelbeds.hackatravel.tmt.controllers;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hotelbeds.hackatravel.tmt.service.ActApiService;
import com.hotelbeds.tab.sdk.model.pojo.Segment;

@RestController
@RequestMapping("/segments")
public class SegmentController {
    @Autowired
    private ActApiService actApiService;
    @PostConstruct
    public void init(){
        getSegments();
    }
    @RequestMapping
    public List<Segment> getSegments() {
        return actApiService.getSegments();
    }
}
