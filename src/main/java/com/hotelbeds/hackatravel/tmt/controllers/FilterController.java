/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelbeds.hackatravel.tmt.controllers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hotelbeds.hackatravel.tmt.filters.FilterCost;
import com.hotelbeds.hackatravel.tmt.filters.FilterGroups;
import com.hotelbeds.hackatravel.tmt.filters.FilterPreferences;
import com.hotelbeds.tab.sdk.model.domain.ActivityFilterType;
import com.hotelbeds.tab.sdk.model.pojo.Activity;
import com.hotelbeds.tab.sdk.model.pojo.ActivitySearchFilterItem;

@RestController
@RequestMapping(path = "/filters")
public class FilterController {
    @Autowired
    private FilterGroups fg;
    @Autowired
    private FilterCost fc;
    @Autowired
    private FilterPreferences fp;

    @RequestMapping
    public List<Activity> getFilter(@RequestBody Map<String, String> map) {
        String select = map.get("Select");
        map.putIfAbsent("From", "2018-03-04");
        map.putIfAbsent("To", "2018-04-01");
        map.putIfAbsent("Latitude", "41.49004");
        map.putIfAbsent("Longitude", "2.08161");
        List<ActivitySearchFilterItem> preFilters = extractFilters(map);
        switch (select) {
            case "Cost":
            case "cost":
                return fc.retrieveWithFilter(map, preFilters);
            case "Activity":
            case "activity":
                return fp.retrieveWithFilter(map, preFilters);
            case "Group":
            case "group":
                return fg.retrieveWithFilter(map, preFilters);
            default:
                return null;
        }
    }

    private List<ActivitySearchFilterItem> extractFilters(Map<String, String> map) {
        List<ActivitySearchFilterItem> filters = new ArrayList<>();
        List<ActivitySearchFilterItem> filtype = new ArrayList<>();
        BigDecimal latitude = new BigDecimal(Double.parseDouble(map.get("Latitude")));
        latitude = latitude.setScale(5, RoundingMode.CEILING);
        BigDecimal longitude = new BigDecimal(Double.parseDouble(map.get("Longitude")));
        longitude = longitude.setScale(5, RoundingMode.CEILING);
        filters.add(new ActivitySearchFilterItem(ActivityFilterType.COORDINATES_GPS, latitude, longitude));
        String lastAction = map.get("LastAction");
        if (lastAction != null && !lastAction.isEmpty()) {
            String[] pairs = lastAction.split("&");
            for (int i = 0; i < pairs.length; ++i) {
                pairs[i] = pairs[i].replaceAll("select=", "").replaceAll("content=", "");
            }
            String[] keys = pairs[0].split(";");
            String[] values = pairs[1].split(";");
            Map<String, String> mp = new HashMap();
            for (int i = 0; i < keys.length; i++) {
                mp.put(keys[i], values[i]);
                mp.put("Select",keys[i]);
                mp.put("Content",values[i]);
                filtype = getTypeOfFilters(mp);
                for (int j = 0; j < filtype.size(); j++) {
                    filters.add(filtype.get(j));
                }
            }
        }
        return filters;
    }

    private List<ActivitySearchFilterItem> getTypeOfFilters(@RequestBody Map<String, String> map) {
        String select = map.get("Select");
        switch (select) {
            case "Cost":
            case "cost":
                return fc.getFilters(map, new ArrayList<>());
            case "Activity":
            case "activity":
                return fp.getFilters(map, new ArrayList<>());
            case "Group":
            case "group":
                return fg.getFilters(map, new ArrayList<>());
            default:
                return null;
        }
    }
}
