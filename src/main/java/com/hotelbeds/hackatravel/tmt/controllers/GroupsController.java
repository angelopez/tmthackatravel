/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelbeds.hackatravel.tmt.controllers;

import com.hotelbeds.hackatravel.tmt.filters.FilterGroups;
import com.hotelbeds.tab.sdk.model.pojo.Activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/groups")
public class GroupsController {
    @Autowired
    private FilterController filterController;
    
    @RequestMapping(path="")
    public List<Activity> list(){
        Map<String,String> map = new HashMap();
        map.put("Select", "Group");
        return filterController.getFilter(map);
    }
}
