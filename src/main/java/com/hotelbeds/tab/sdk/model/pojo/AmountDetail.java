package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * Created by mcazorla on 11/02/16.
 *
 */

@XmlType(name = "AmountDetail", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "totalAmount", "paxAmounts"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class AmountDetail {

    
    private List<PaxPrice>         paxAmounts;

    
    private PaxPrice               totalAmount;

    public List<PaxPrice> getPaxAmounts() {
        return paxAmounts;
    }

    public void setPaxAmounts(List<PaxPrice> paxAmounts) {
        this.paxAmounts = paxAmounts;
    }

    public PaxPrice getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(PaxPrice totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.paxAmounts);
        hash = 89 * hash + Objects.hashCode(this.totalAmount);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AmountDetail other = (AmountDetail) obj;
        if (!Objects.equals(this.paxAmounts, other.paxAmounts)) {
            return false;
        }
        if (!Objects.equals(this.totalAmount, other.totalAmount)) {
            return false;
        }
        return true;
    }
    
    

}
