package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Objects;


@XmlType(name = "PaymentData", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages", propOrder = {"paymentType", "invoicingCompany","description"})
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentData implements Serializable {

    private static final long serialVersionUID = 1L;

    
    private PaymentType       paymentType;

    
    private InvoiceCompany    invoicingCompany;

    
    private String            description;

    public PaymentData() {
        super();
    }

    public PaymentData(PaymentType paymentType, InvoiceCompany invoicingCompany, String description) {
        super();
        this.paymentType = paymentType;
        this.invoicingCompany = invoicingCompany;
        this.description = description;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public InvoiceCompany getInvoicingCompany() {
        return invoicingCompany;
    }

    public void setInvoicingCompany(InvoiceCompany invoicingCompany) {
        this.invoicingCompany = invoicingCompany;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "OSCPaymentData [paymentType=" + paymentType + ", invoicingCompany=" + invoicingCompany + ", description=" + description + "]";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.paymentType);
        hash = 97 * hash + Objects.hashCode(this.invoicingCompany);
        hash = 97 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PaymentData other = (PaymentData) obj;
        if (!Objects.equals(this.paymentType, other.paymentType)) {
            return false;
        }
        if (!Objects.equals(this.invoicingCompany, other.invoicingCompany)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return true;
    }
    
    

}
