package com.hotelbeds.tab.sdk.model.pojo;

import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlType(name = "RoutePoint",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "type", "order", "stop", "description", "descriptions", "pointOfInterest", "stopTime", "timeFrom", "timeTo"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class RoutePoint implements Comparable<RoutePoint> {

    
    private String                   type;
    
    private Integer                  order;
    
    private boolean                  stop;
    
    private String                   description;
    
    private List<DuoTypeDescription> descriptions;
    
    private PointOfInterest          pointOfInterest;
    
    private Duration                 stopTime;
    
    private String                   timeFrom; //HH:MM
    
    private String                   timeTo; //HH:MM

    public RoutePoint() {
        super();
    }

    public RoutePoint(String description, List<DuoTypeDescription> descriptions, Integer order, PointOfInterest pointOfInterest, boolean stop, Duration stopTime, String timeFrom, String timeTo, String type) {
        super();
        this.description = description;
        this.descriptions = descriptions;
        this.order = order;
        this.pointOfInterest = pointOfInterest;
        this.stop = stop;
        this.stopTime = stopTime;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<DuoTypeDescription> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DuoTypeDescription> descriptions) {
        this.descriptions = descriptions;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public PointOfInterest getPointOfInterest() {
        return pointOfInterest;
    }

    public void setPointOfInterest(PointOfInterest pointOfInterest) {
        this.pointOfInterest = pointOfInterest;
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public Duration getStopTime() {
        return stopTime;
    }

    public void setStopTime(Duration stopTime) {
        this.stopTime = stopTime;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int compareTo(RoutePoint o) {
        return this.order.compareTo(o.order);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Type=").append(type).append(",");
        sb.append("Order=").append(order).append(",");
        sb.append("Stop=").append(stop).append(",");
        sb.append("Description=").append(description).append(",");
        sb.append("Descriptions=").append(descriptions).append(",");
        sb.append("PointOfInterest=").append(pointOfInterest).append(",");
        sb.append("StopTime=").append(stopTime).append(",");
        sb.append("TimeFrom=").append(timeFrom).append(",");
        sb.append("TimeTo=").append(timeTo);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.type);
        hash = 89 * hash + Objects.hashCode(this.order);
        hash = 89 * hash + (this.stop ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.description);
        hash = 89 * hash + Objects.hashCode(this.descriptions);
        hash = 89 * hash + Objects.hashCode(this.pointOfInterest);
        hash = 89 * hash + Objects.hashCode(this.stopTime);
        hash = 89 * hash + Objects.hashCode(this.timeFrom);
        hash = 89 * hash + Objects.hashCode(this.timeTo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RoutePoint other = (RoutePoint) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.order, other.order)) {
            return false;
        }
        if (this.stop != other.stop) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.descriptions, other.descriptions)) {
            return false;
        }
        if (!Objects.equals(this.pointOfInterest, other.pointOfInterest)) {
            return false;
        }
        if (!Objects.equals(this.stopTime, other.stopTime)) {
            return false;
        }
        if (!Objects.equals(this.timeFrom, other.timeFrom)) {
            return false;
        }
        if (!Objects.equals(this.timeTo, other.timeTo)) {
            return false;
        }
        return true;
    }
    
    

}
