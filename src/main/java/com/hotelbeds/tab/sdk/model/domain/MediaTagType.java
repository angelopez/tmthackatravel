package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "MediaTagType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum MediaTagType
{
    MAP("MAP"),
    LEAFLET("LEAFLET"),
    RECOMMENDATIONS("RECOMMENDATIONS"),
    ADVANCED_TIPS("ADVANCED_TIPS"),
    OTHER("OTHER");
    
    private String code;

    MediaTagType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static MediaTagType fromCode(String code) {
        for (MediaTagType type : MediaTagType.values()) {
            if (type.getCode().equalsIgnoreCase(code)) {
                return type;
            }
        }
        return OTHER;
    }
}
