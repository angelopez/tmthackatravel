package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@XmlType(name = "Route")
@XmlAccessorType(XmlAccessType.FIELD)
public class Route {

    
    private Duration         duration;
    
    private String           description;
    
    private String           timeFrom;
    
    private String           timeTo;
    
    private List<RoutePoint> points;
    
    private Frequency        frequency;

    public Route() {
        super();
    }

    public Route(String code, String description, Duration duration, String timeFrom, String timeTo, List<RoutePoint> points, Frequency frequency) {
        super();
        this.description = description;
        this.duration = duration;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.points = points;
        this.frequency = frequency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public List<RoutePoint> getPoints() {
        return points;
    }

    public void setPoints(List<RoutePoint> points) {
        this.points = points;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Duration=").append(duration).append(",");
        sb.append("Description=").append(description).append(",");
        sb.append("TimeFrom=").append(timeFrom).append(",");
        sb.append("TimeTo=").append(timeTo).append(",");
        sb.append("Points=").append(points);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.duration);
        hash = 37 * hash + Objects.hashCode(this.description);
        hash = 37 * hash + Objects.hashCode(this.timeFrom);
        hash = 37 * hash + Objects.hashCode(this.timeTo);
        hash = 37 * hash + Objects.hashCode(this.points);
        hash = 37 * hash + Objects.hashCode(this.frequency);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Route other = (Route) obj;
        if (!Objects.equals(this.duration, other.duration)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.timeFrom, other.timeFrom)) {
            return false;
        }
        if (!Objects.equals(this.timeTo, other.timeTo)) {
            return false;
        }
        if (!Objects.equals(this.points, other.points)) {
            return false;
        }
        if (!Objects.equals(this.frequency, other.frequency)) {
            return false;
        }
        return true;
    }
    
    

}
