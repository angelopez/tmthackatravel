package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import com.hotelbeds.tab.sdk.model.domain.WeekDayType;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@XmlType(name = "ScheduleInfo",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "from", "to", "weekDays", "openingTime", "closeTime", "lastAdmissionTime" } )
@XmlAccessorType(XmlAccessType.FIELD)
public class ScheduleInfo {

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date              from;
    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date              to;
    
    private String            openingTime; //HHMM
    
    private String            closeTime; //HHMM
    
    private String            lastAdmissionTime; //HHMM
    
    private List<WeekDayType> weekDays;

    public ScheduleInfo() {
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public List<WeekDayType> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(List<WeekDayType> weekDays) {
        this.weekDays = weekDays;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getLastAdmissionTime() {
        return lastAdmissionTime;
    }

    public void setLastAdmissionTime(String lastAdmissionTime) {
        this.lastAdmissionTime = lastAdmissionTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("From=").append(from).append(",");
        sb.append("To=").append(to).append(",");
        sb.append("WeekDays=").append(weekDays);
        sb.append("OpeningTime=").append(openingTime);
        sb.append("ClosingTime=").append(closeTime);
        sb.append("LastAdmissionTime=").append(lastAdmissionTime);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.from);
        hash = 29 * hash + Objects.hashCode(this.to);
        hash = 29 * hash + Objects.hashCode(this.openingTime);
        hash = 29 * hash + Objects.hashCode(this.closeTime);
        hash = 29 * hash + Objects.hashCode(this.lastAdmissionTime);
        hash = 29 * hash + Objects.hashCode(this.weekDays);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ScheduleInfo other = (ScheduleInfo) obj;
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.openingTime, other.openingTime)) {
            return false;
        }
        if (!Objects.equals(this.closeTime, other.closeTime)) {
            return false;
        }
        if (!Objects.equals(this.lastAdmissionTime, other.lastAdmissionTime)) {
            return false;
        }
        if (!Objects.equals(this.weekDays, other.weekDays)) {
            return false;
        }
        return true;
    }
    
    

}
