package com.hotelbeds.tab.sdk.model.request;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import com.hotelbeds.tab.sdk.model.pojo.HotelCodePickupRequest;
import com.hotelbeds.tab.sdk.model.pojo.PaginationRequest;
import java.util.Date;
import java.util.Objects;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "RetrieveExcursionPickupsRequest")
@XmlType(name = "RetrieveExcursionPickupsRequest",
        namespace = "com.hotelbeds.tab.actapi.v3.request.RetrieveExcursionPickupsRequest",
        propOrder = {"pickupRetrievalKey", "from", "to", "sessionCode", "languageCode"})
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveExcursionPickupsRequest {

    
    private String pickupRetrievalKey;

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date from;

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date to;

    
    private String sessionCode;

    
    private String languageCode;

    public RetrieveExcursionPickupsRequest() {
        super();
    }

    public RetrieveExcursionPickupsRequest(PaginationRequest pagination, String pickupRetrievalKey, HotelCodePickupRequest hotelCode, Date from, Date to, String sessionCode, String languageCode, String zoneCode) {
        super();
        this.pickupRetrievalKey = pickupRetrievalKey;
        this.from = from;
        this.to = to;
        this.sessionCode = sessionCode;
        this.languageCode = languageCode;
    }

    public String getPickupRetrievalKey() {
        return pickupRetrievalKey;
    }

    public void setPickupRetrievalKey(String pickupRetrievalKey) {
        this.pickupRetrievalKey = pickupRetrievalKey;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getSessionCode() {
        return sessionCode;
    }

    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PickupRetrievalKey=").append(pickupRetrievalKey).append(",");
        sb.append("from=").append(from).append(",");
        sb.append("to=").append(to).append(",");

        sb.append("SessionCode=").append(sessionCode).append(",");
        sb.append("LanguageCode=").append(languageCode).append(",");

        sb.append("class=RetrieveExcursionPickupsRequest");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.pickupRetrievalKey);
        hash = 59 * hash + Objects.hashCode(this.from);
        hash = 59 * hash + Objects.hashCode(this.to);
        hash = 59 * hash + Objects.hashCode(this.sessionCode);
        hash = 59 * hash + Objects.hashCode(this.languageCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RetrieveExcursionPickupsRequest other = (RetrieveExcursionPickupsRequest) obj;
        if (!Objects.equals(this.pickupRetrievalKey, other.pickupRetrievalKey)) {
            return false;
        }
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.sessionCode, other.sessionCode)) {
            return false;
        }
        if (!Objects.equals(this.languageCode, other.languageCode)) {
            return false;
        }
        return true;
    }

    

}
