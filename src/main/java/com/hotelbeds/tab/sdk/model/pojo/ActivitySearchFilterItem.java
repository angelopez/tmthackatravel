package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.ActivityFilterTypeAdapter;
import com.hotelbeds.tab.sdk.model.domain.ActivityFilterType;
import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "ActivitySearchFilterItem", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages" )
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivitySearchFilterItem  {

    
    @XmlJavaTypeAdapter(ActivityFilterTypeAdapter.class)
    private String type;
    
    private String             value;
    
    private BigDecimal         latitude;
    
    private BigDecimal         longitude;

    public ActivitySearchFilterItem() {
    }
    
    public ActivitySearchFilterItem(ActivityFilterType type, String value) {
        super();
        this.type = type.getType();

        this.value = value;
    }

    public ActivitySearchFilterItem(ActivityFilterType type, BigDecimal latitude, BigDecimal longitude) {
        super();
        this.type = type.getType();
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setType(ActivityFilterType type) {
        this.type = type.getType();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "ActivitySearchFilterItem{" +
                "type=" + type +
                ", value='" + value + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.type);
        hash = 71 * hash + Objects.hashCode(this.value);
        hash = 71 * hash + Objects.hashCode(this.latitude);
        hash = 71 * hash + Objects.hashCode(this.longitude);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActivitySearchFilterItem other = (ActivitySearchFilterItem) obj;
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        return true;
    }
    
    
    

}
