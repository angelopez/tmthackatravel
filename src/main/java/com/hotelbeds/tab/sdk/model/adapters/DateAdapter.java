package com.hotelbeds.tab.sdk.model.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.springframework.util.StringUtils;

public class DateAdapter extends XmlAdapter<String, Date> {
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date unmarshal(String v) throws Exception {
        if (StringUtils.isEmpty(v)) {
            return null;
        } else {
            return sdf.parse(v);
        }
    }

    @Override
    public String marshal(Date v) throws Exception {
        return v != null ? sdf.format(v) : null;
    }
}
