package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.SegmentGroup;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "SegmentsResponse")
@XmlType(name = "SegmentsResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.SegmentsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class SegmentsResponse extends BaseResponse {

    
    private List<SegmentGroup> segmentationGroups;

    
    private PaginationResponse pagination;

    public PaginationResponse getPagination() {
        return pagination;
    }

    public void setPagination(PaginationResponse pagination) {
        this.pagination = pagination;
    }

    public List<SegmentGroup> getSegmentationGroups() {
        return segmentationGroups;
    }

    public void setSegmentationGroups(List<SegmentGroup> segmentationGroups) {
        this.segmentationGroups = segmentationGroups;
    }

    @Override
    public String toString() {
        //TODO: Falta por determinar los atributos que se muestran. Primera propuesta
        StringBuilder sb = new StringBuilder();
        if (segmentationGroups != null) {
            sb.append("NumOfSegmentationsGroups=" + segmentationGroups.size()).append(",");
        } else {
            sb.append("NumOfSegmentationsGroups=0").append(",");
        }
        sb.append("class=SegmentsResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.segmentationGroups);
        hash = 17 * hash + Objects.hashCode(this.pagination);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final SegmentsResponse other = (SegmentsResponse) obj;
        if (!Objects.equals(this.segmentationGroups, other.segmentationGroups)) {
            return false;
        }
        if (!Objects.equals(this.pagination, other.pagination)) {
            return false;
        }
        return true;
    }

}
