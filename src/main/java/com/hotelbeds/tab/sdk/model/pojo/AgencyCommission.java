package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.Objects;

@XmlType(name = "AgencyCommission", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "percentage", "amount", "vatAmount", "vatPercentage"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class AgencyCommission {

    
    private BigDecimal percentage;

    
    private BigDecimal amount;

    
    private BigDecimal vatPercentage;

    
    private BigDecimal vatAmount;

    public AgencyCommission() {
        super();
    }

    public AgencyCommission(BigDecimal percentage, BigDecimal amount, BigDecimal vatAmount) {
        super();
        this.percentage = percentage;
        this.amount = amount;
        this.vatAmount = vatAmount;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getVatPercentage() {
        return vatPercentage;
    }

    public void setVatPercentage(BigDecimal vatPercentage) {
        this.vatPercentage = vatPercentage;
    }

    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    @Override
    public String toString() {
        return "AgencyCommission [percentage=" + percentage + ", amount=" + amount + ", vatAmount=" + vatAmount + "]";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.percentage);
        hash = 23 * hash + Objects.hashCode(this.amount);
        hash = 23 * hash + Objects.hashCode(this.vatPercentage);
        hash = 23 * hash + Objects.hashCode(this.vatAmount);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AgencyCommission other = (AgencyCommission) obj;
        if (!Objects.equals(this.percentage, other.percentage)) {
            return false;
        }
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        if (!Objects.equals(this.vatPercentage, other.vatPercentage)) {
            return false;
        }
        if (!Objects.equals(this.vatAmount, other.vatAmount)) {
            return false;
        }
        return true;
    }

    
    
}
