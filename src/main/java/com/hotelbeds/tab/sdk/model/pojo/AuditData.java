package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateTimeAdapter;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "AuditData",
         propOrder = { "serverId", "environment", "processTime", "time", "ACErequests", "ACEResponses" },
         namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages" )
@XmlAccessorType(XmlAccessType.FIELD)
public class AuditData {

    
    private Double processTime;

    
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private Date time;

    
    private String serverId;

    
    private String environment;

    
    private List<String> ACErequests;

    
    private List<String> ACEResponses;

    public Double getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Double processTime) {
        this.processTime = processTime;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public List<String> getACErequests() {
        return ACErequests;
    }

    public void setACErequests(List<String> ACErequests) {
        this.ACErequests = ACErequests;
    }

    public List<String> getACEResponses() {
        return ACEResponses;
    }

    public void setACEResponses(List<String> ACEResponses) {
        this.ACEResponses = ACEResponses;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.processTime);
        hash = 59 * hash + Objects.hashCode(this.time);
        hash = 59 * hash + Objects.hashCode(this.serverId);
        hash = 59 * hash + Objects.hashCode(this.environment);
        hash = 59 * hash + Objects.hashCode(this.ACErequests);
        hash = 59 * hash + Objects.hashCode(this.ACEResponses);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuditData other = (AuditData) obj;
        if (!Objects.equals(this.processTime, other.processTime)) {
            return false;
        }
        if (!Objects.equals(this.time, other.time)) {
            return false;
        }
        if (!Objects.equals(this.serverId, other.serverId)) {
            return false;
        }
        if (!Objects.equals(this.environment, other.environment)) {
            return false;
        }
        if (!Objects.equals(this.ACErequests, other.ACErequests)) {
            return false;
        }
        if (!Objects.equals(this.ACEResponses, other.ACEResponses)) {
            return false;
        }
        return true;
    }
    
    
}
