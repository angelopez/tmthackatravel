package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateTimeAdapter;
import com.hotelbeds.tab.sdk.model.domain.BookingStatus;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "Booking")
@XmlAccessorType(XmlAccessType.FIELD)
public class Booking {

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private Date creationDate;
    private String creationUser;
    private PaymentData paymentData;
    private String clientReference;
    private BookingHolder holder;
    private BigDecimal total;
    private BigDecimal totalNet;
    private List<PurchasedService> activities;
    private String reference;
    private BookingStatus status;
    private String currency;
    private BigDecimal pendingAmount;
    private PaymentInformationResponse paymentInformation;
    private List<BookingTransaction> transactions;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public PaymentData getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(PaymentData paymentData) {
        this.paymentData = paymentData;
    }

    public String getClientReference() {
        return clientReference;
    }

    public void setClientReference(String clientReference) {
        this.clientReference = clientReference;
    }

    public BookingHolder getHolder() {
        return holder;
    }

    public void setHolder(BookingHolder holder) {
        this.holder = holder;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTotalNet() {
        return totalNet;
    }

    public void setTotalNet(BigDecimal totalNet) {
        this.totalNet = totalNet;
    }

    public List<PurchasedService> getActivities() {
        return activities;
    }

    public void setActivities(List<PurchasedService> activities) {
        this.activities = activities;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(BigDecimal pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public PaymentInformationResponse getPaymentInformation() {
        return paymentInformation;
    }

    public void setPaymentInformation(PaymentInformationResponse paymentInformation) {
        this.paymentInformation = paymentInformation;
    }

    public List<BookingTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<BookingTransaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.creationDate);
        hash = 31 * hash + Objects.hashCode(this.creationUser);
        hash = 31 * hash + Objects.hashCode(this.paymentData);
        hash = 31 * hash + Objects.hashCode(this.clientReference);
        hash = 31 * hash + Objects.hashCode(this.holder);
        hash = 31 * hash + Objects.hashCode(this.total);
        hash = 31 * hash + Objects.hashCode(this.totalNet);
        hash = 31 * hash + Objects.hashCode(this.activities);
        hash = 31 * hash + Objects.hashCode(this.reference);
        hash = 31 * hash + Objects.hashCode(this.status);
        hash = 31 * hash + Objects.hashCode(this.currency);
        hash = 31 * hash + Objects.hashCode(this.pendingAmount);
        hash = 31 * hash + Objects.hashCode(this.paymentInformation);
        hash = 31 * hash + Objects.hashCode(this.transactions);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Booking other = (Booking) obj;
        if (!Objects.equals(this.creationDate, other.creationDate)) {
            return false;
        }
        if (!Objects.equals(this.creationUser, other.creationUser)) {
            return false;
        }
        if (!Objects.equals(this.paymentData, other.paymentData)) {
            return false;
        }
        if (!Objects.equals(this.clientReference, other.clientReference)) {
            return false;
        }
        if (!Objects.equals(this.holder, other.holder)) {
            return false;
        }
        if (!Objects.equals(this.total, other.total)) {
            return false;
        }
        if (!Objects.equals(this.totalNet, other.totalNet)) {
            return false;
        }
        if (!Objects.equals(this.activities, other.activities)) {
            return false;
        }
        if (!Objects.equals(this.reference, other.reference)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        if (!Objects.equals(this.pendingAmount, other.pendingAmount)) {
            return false;
        }
        if (!Objects.equals(this.paymentInformation, other.paymentInformation)) {
            return false;
        }
        if (!Objects.equals(this.transactions, other.transactions)) {
            return false;
        }
        return true;
    }

}
