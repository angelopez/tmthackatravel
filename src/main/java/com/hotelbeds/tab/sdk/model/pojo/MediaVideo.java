package com.hotelbeds.tab.sdk.model.pojo;

import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "MediaVideo")
public class MediaVideo extends MediaBase {

    private List<String> language;

    public MediaVideo() {
        super();
    }

    public MediaVideo(List<String> language) {
        this.language = language;
    }

    public List<String> getLanguage() {
        return language;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.language);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final MediaVideo other = (MediaVideo) obj;
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        return true;
    }
    
}
