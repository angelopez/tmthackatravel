package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.ActivityType;
import javax.xml.bind.annotation.*;
import java.util.List;

@XmlType(name = "PurchasableActivity")
public class PurchasableActivity  {

    private String activityCode;
    private String url;
    private Country country;
    private List<OperationDay> operationDays;
    private List<Modality> modalities;
    private Integer originServices;
    private String currencyName;
    private List<PaxPrice> amountsFrom;
    private FactsheetActivity content;
    private String name;
    private String currency;
    private String code;
    private String contentId;
    private ActivityType type;

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<OperationDay> getOperationDays() {
        return operationDays;
    }

    public void setOperationDays(List<OperationDay> operationDays) {
        this.operationDays = operationDays;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Modality> getModalities() {
        return modalities;
    }

    public void setModalities(List<Modality> modalities) {
        this.modalities = modalities;
    }

    public Integer getOriginServices() {
        return originServices;
    }

    public List<PaxPrice> getAmountsFrom() {
        return amountsFrom;
    }

    public void setAmountsFrom(List<PaxPrice> amountsFrom) {
        this.amountsFrom = amountsFrom;
    }

    public void setOriginServices(Integer originServices) {
        this.originServices = originServices;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public FactsheetActivity getContent() {
        return content;
    }

    public void setContent(FactsheetActivity content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public ActivityType getType() {
        return type;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }
    
    

}
