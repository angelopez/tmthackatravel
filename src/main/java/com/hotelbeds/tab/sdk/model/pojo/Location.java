package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import java.util.Objects;

@XmlType(name = "Location",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "endPoints", "startingPoints"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Location {

    
    private List<LocationPoint> endPoints;
    
    private List<LocationPoint> startingPoints;

    public Location() {
    }

    public List<LocationPoint> getEndPoints() {
        return endPoints;
    }

    public void setEndPoints(List<LocationPoint> endPoints) {
        this.endPoints = endPoints;
    }

    public List<LocationPoint> getStartingPoints() {
        return startingPoints;
    }

    public void setStartingPoints(List<LocationPoint> startingPoints) {
        this.startingPoints = startingPoints;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EndPoints=").append(endPoints).append(",");
        sb.append("StartPoints=").append(startingPoints);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.endPoints);
        hash = 41 * hash + Objects.hashCode(this.startingPoints);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Location other = (Location) obj;
        if (!Objects.equals(this.endPoints, other.endPoints)) {
            return false;
        }
        if (!Objects.equals(this.startingPoints, other.startingPoints)) {
            return false;
        }
        return true;
    }
    
    

}
