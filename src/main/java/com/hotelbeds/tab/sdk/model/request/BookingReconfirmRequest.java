package com.hotelbeds.tab.sdk.model.request;

import com.hotelbeds.tab.sdk.model.pojo.BookingTransaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "BookingReconfirmRequest")
public class BookingReconfirmRequest {

    //TODO RECOVERY
//    private List<BookingTransaction> transactions;
    private String reference;
    private String language;

    public BookingReconfirmRequest() {
//    this.transactions = new ArrayList<BookingTransaction>();
    }

    public BookingReconfirmRequest(List<BookingTransaction> transactions,
            String reference, String language) {
        super();
//		this.transactions = transactions;
        this.reference = reference;
        this.language = language;
    }

//    public List<BookingTransaction> getTransactions() {
//        return transactions;
//    }
//
//    public void setTransactions(List<BookingTransaction> transactions) {
//        this.transactions = transactions;
//    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public String toString() {
        //TODO: Determinar los atributos que mostrar. Primera Propuesta
        StringBuilder sb = new StringBuilder();
        sb.append("BookingReference=" + getReference()).append(",");
//        if (transactions != null) {
//            sb.append("NumOfTransactions=" + transactions.size()).append(",");
//        } else {
//            sb.append("NumOfTransactions=0").append(",");
//        }
        sb.append("class=BookingReconfirmRequest");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.reference);
        hash = 17 * hash + Objects.hashCode(this.language);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookingReconfirmRequest other = (BookingReconfirmRequest) obj;
        if (!Objects.equals(this.reference, other.reference)) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        return true;
    }

    
    
}
