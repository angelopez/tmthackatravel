package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.ModalityUnitType;
import java.util.Objects;

/**
 * Created by mcazorla on 30/03/16.
 */
public class ConfirmationCode {

    private String code;

    private ModalityUnitType unitType;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ModalityUnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(ModalityUnitType unitType) {
        this.unitType = unitType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.code);
        hash = 73 * hash + Objects.hashCode(this.unitType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConfirmationCode other = (ConfirmationCode) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (this.unitType != other.unitType) {
            return false;
        }
        return true;
    }
    
    
}
