package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "Frequency",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = {"maximum", "minimum"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Frequency {

    
    private Duration maximum;
    
    private Duration minimum;

    public Frequency() {
        super();
    }

    public Frequency(Duration maximum, Duration minimum) {
        super();
        this.maximum = maximum;
        this.minimum = minimum;
    }

    public Duration getMaximum() {
        return maximum;
    }

    public void setMaximum(Duration maximum) {
        this.maximum = maximum;
    }

    public Duration getMinimum() {
        return minimum;
    }

    public void setMinimum(Duration minimum) {
        this.minimum = minimum;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Maximum=").append(maximum).append(",");
        sb.append("Minimum=").append(minimum);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.maximum);
        hash = 83 * hash + Objects.hashCode(this.minimum);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Frequency other = (Frequency) obj;
        if (!Objects.equals(this.maximum, other.maximum)) {
            return false;
        }
        if (!Objects.equals(this.minimum, other.minimum)) {
            return false;
        }
        return true;
    }
    
    

}
