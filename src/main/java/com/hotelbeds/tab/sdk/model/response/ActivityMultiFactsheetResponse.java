package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.FactsheetActivity;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name="ActivityMultiFactsheetResponse")
@XmlType(name = "ActivityMultiFactsheetResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.ActivityFactsheetResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivityMultiFactsheetResponse extends BaseResponse {

    
    private List<FactsheetActivity> activitiesContent;

    public ActivityMultiFactsheetResponse() {

    }

    public List<FactsheetActivity> getActivitiesContent() {
        return activitiesContent;
    }

    public void setActivitiesContent(List<FactsheetActivity> activitiesContent) {
        this.activitiesContent = activitiesContent;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString()); // auditData

        sb.append("class=ActivityMultiFactsheetResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.activitiesContent);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final ActivityMultiFactsheetResponse other = (ActivityMultiFactsheetResponse) obj;
        if (!Objects.equals(this.activitiesContent, other.activitiesContent)) {
            return false;
        }
        return true;
    }
    
    
}
