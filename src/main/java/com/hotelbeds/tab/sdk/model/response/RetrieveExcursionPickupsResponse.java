package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.ActivityPickup;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "RetrieveExcursionPickupsResponse")
@XmlType(name = "RetrieveExcursionPickupsResponse",
        namespace = "com.hotelbeds.tab.actapi.v3.response.RetrieveExcursionPickupsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveExcursionPickupsResponse extends BaseResponse {

    
    private List<ActivityPickup> pickups;

    
    private PaginationResponse pagination;

    public PaginationResponse getPagination() {
        return pagination;
    }

    public void setPagination(PaginationResponse pagination) {
        this.pagination = pagination;
    }

    public List<ActivityPickup> getPickups() {
        return pickups;
    }

    public void setPickups(List<ActivityPickup> pickups) {
        this.pickups = pickups;
    }

    @Override
    public String toString() {
        //TODO: Determinar los valores que se deben mostrar. Propuesta inicial
        StringBuilder sb = new StringBuilder(super.toString()); // auditData
        if (pickups != null) {
            sb.append("NumPickups=").append(pickups.size()).append(",");
        } else {
            sb.append("NumPickups=0").append(",");
        }

        sb.append("class=RetrieveExcursionPickupsResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.pickups);
        hash = 97 * hash + Objects.hashCode(this.pagination);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final RetrieveExcursionPickupsResponse other = (RetrieveExcursionPickupsResponse) obj;
        if (!Objects.equals(this.pickups, other.pickups)) {
            return false;
        }
        if (!Objects.equals(this.pagination, other.pagination)) {
            return false;
        }
        return true;
    }
    
    
}
