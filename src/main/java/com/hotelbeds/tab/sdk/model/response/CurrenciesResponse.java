package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.DuoTypeName;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "CurrenciesResponse")
@XmlType(name = "CurrenciesResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.CurrenciesResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrenciesResponse extends BaseResponse {

    
    private List<DuoTypeName> currencies;

    public List<DuoTypeName> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<DuoTypeName> currencies) {
        this.currencies = currencies;
    }

    @Override
    public String toString() {
        //TODO: Falta por determinar los atributos que se muestran. Primera propuesta
        StringBuilder sb = new StringBuilder();
        if (currencies != null) {
            sb.append("NumOfCurrencies=" + currencies.size()).append(",");
        } else {
            sb.append("NumOfCurrencies=0").append(",");
        }
        sb.append("class=CurrenciesResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.currencies);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final CurrenciesResponse other = (CurrenciesResponse) obj;
        if (!Objects.equals(this.currencies, other.currencies)) {
            return false;
        }
        return true;
    }
    
    
}
