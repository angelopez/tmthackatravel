package com.hotelbeds.tab.sdk.model.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.hotelbeds.tab.sdk.model.domain.MediaMimeType;

public class MediaMimeTypeAdapter extends XmlAdapter<String, MediaMimeType> {

    @Override
    public MediaMimeType unmarshal(String v) throws Exception {
        if (v != null) {
            return null;
        } else {
            return MediaMimeType.fromCode(v);
        }
    }

    @Override
    public String marshal(MediaMimeType v) throws Exception {
        return v != null ? v.getCode() : null;
    }

}
