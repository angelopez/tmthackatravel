package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by xbibiloni on 16/03/2016.
 *
 */
@XmlEnum
@XmlType(name = "MetricType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum MetricType {
    DAYS,
    HOURS,
    MINUTES
}
