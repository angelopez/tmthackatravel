package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import java.util.Objects;

@XmlType(name = "PriceBreakdown",
         namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
         propOrder = {"rateSupplements","rateDiscounts","rateFees"})
@XmlAccessorType(XmlAccessType.FIELD)
public class PriceBreakdown {

    
    private List<Supplement> rateSupplements;

    
    private List<Supplement> rateDiscounts;

    
    private List<Fee>        rateFees;

    public List<Supplement> getRateSupplements() {
        return rateSupplements;
    }

    public void setRateSupplements(List<Supplement> rateSupplements) {
        this.rateSupplements = rateSupplements;
    }

    public List<Fee> getRateFees() {
        return rateFees;
    }

    public void setRateFees(List<Fee> rateFees) {
        this.rateFees = rateFees;
    }

    public List<Supplement> getRateDiscounts() {
        return rateDiscounts;
    }

    public void setRateDiscounts(List<Supplement> rateDiscounts) {
        this.rateDiscounts = rateDiscounts;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.rateSupplements);
        hash = 11 * hash + Objects.hashCode(this.rateDiscounts);
        hash = 11 * hash + Objects.hashCode(this.rateFees);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PriceBreakdown other = (PriceBreakdown) obj;
        if (!Objects.equals(this.rateSupplements, other.rateSupplements)) {
            return false;
        }
        if (!Objects.equals(this.rateDiscounts, other.rateDiscounts)) {
            return false;
        }
        if (!Objects.equals(this.rateFees, other.rateFees)) {
            return false;
        }
        return true;
    }
    
    

}
