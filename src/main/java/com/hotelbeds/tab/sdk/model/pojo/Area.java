package com.hotelbeds.tab.sdk.model.pojo;


import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.util.Objects;

@XmlType(name = "Area",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "code", "geolocation", "description"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Area {

    
    private String     code;

    
    private Coordinate geolocation;

    
    private String     description;

    public Area() {
    }

    public Area(String code, Coordinate geolocation, String description) {
        this.code = code;
        this.geolocation = geolocation;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Coordinate getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Coordinate geolocation) {
        this.geolocation = geolocation;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Code=").append(code).append(",");
        sb.append("Description=").append(description).append(",");
        sb.append("Geolocation=").append(geolocation).append(",");

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.code);
        hash = 29 * hash + Objects.hashCode(this.geolocation);
        hash = 29 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Area other = (Area) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.geolocation, other.geolocation)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return true;
    }
    
    

}
