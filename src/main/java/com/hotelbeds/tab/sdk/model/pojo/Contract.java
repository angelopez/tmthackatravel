package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;

public class Contract {

    private Integer incommingOffice;
    private String  name;

    public Integer getIncommingOffice() {
        return incommingOffice;
    }

    public void setIncommingOffice(Integer incommingOffice) {
        this.incommingOffice = incommingOffice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.incommingOffice);
        hash = 17 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contract other = (Contract) obj;
        if (!Objects.equals(this.incommingOffice, other.incommingOffice)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    

}
