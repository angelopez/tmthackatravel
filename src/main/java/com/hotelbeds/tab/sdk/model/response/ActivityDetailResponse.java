package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.PurchasableActivity;
import java.util.Objects;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "ActivityDetailResponse")
public class ActivityDetailResponse extends BaseResponse {

    private PurchasableActivity activity;

    public PurchasableActivity getActivity() {
        return activity;
    }

    public void setActivity(PurchasableActivity activity) {
        this.activity = activity;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.activity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final ActivityDetailResponse other = (ActivityDetailResponse) obj;
        if (!Objects.equals(this.activity, other.activity)) {
            return false;
        }
        return true;
    }

    
    
}
