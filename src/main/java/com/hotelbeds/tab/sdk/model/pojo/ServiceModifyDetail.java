package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ServiceModifyDetail")
public class ServiceModifyDetail extends BookingConfirmService {

    private static final long serialVersionUID = 1L;
    private Long hotelCode;

    public Long getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(Long hotelCode) {
        this.hotelCode = hotelCode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.hotelCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final ServiceModifyDetail other = (ServiceModifyDetail) obj;
        if (!Objects.equals(this.hotelCode, other.hotelCode)) {
            return false;
        }
        return true;
    }
    
    

}
