package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@XmlType(name = "SearchModality",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = {"code", "name", "duration", "amountsFrom", "cancellationPolicies", "specificContent"})
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchModality {

    
    private String code;
    
    private String name;
    
    private Duration duration;
    
    private List<PaxPrice> amountsFrom;
    
    private List<CancellationPolicy> cancellationPolicies;
    
    private FactsheetActivity specificContent;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public List<PaxPrice> getAmountsFrom() {
        return amountsFrom;
    }

    public void setAmountsFrom(List<PaxPrice> amountsFrom) {
        this.amountsFrom = amountsFrom;
    }

    public List<CancellationPolicy> getCancellationPolicies() {
        return cancellationPolicies;
    }

    public void setCancellationPolicies(List<CancellationPolicy> cancellationPolicies) {
        this.cancellationPolicies = cancellationPolicies;
    }

    public FactsheetActivity getSpecificContent() {
        return specificContent;
    }

    public void setSpecificContent(FactsheetActivity specificContent) {
        this.specificContent = specificContent;
    }

    @Override
    public String toString() {
        return "SearchModality{"
                + "code='" + code + '\''
                + ", duration=" + duration
                + ", name='" + name + '\''
                + ", amountsFrom=" + amountsFrom
                + ", cancellationPolicies=" + cancellationPolicies
                + ", specificContent=" + specificContent
                + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.code);
        hash = 41 * hash + Objects.hashCode(this.name);
        hash = 41 * hash + Objects.hashCode(this.duration);
        hash = 41 * hash + Objects.hashCode(this.amountsFrom);
        hash = 41 * hash + Objects.hashCode(this.cancellationPolicies);
        hash = 41 * hash + Objects.hashCode(this.specificContent);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SearchModality other = (SearchModality) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.duration, other.duration)) {
            return false;
        }
        if (!Objects.equals(this.amountsFrom, other.amountsFrom)) {
            return false;
        }
        if (!Objects.equals(this.cancellationPolicies, other.cancellationPolicies)) {
            return false;
        }
        if (!Objects.equals(this.specificContent, other.specificContent)) {
            return false;
        }
        return true;
    }
    
    
}
