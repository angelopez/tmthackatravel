package  com.hotelbeds.tab.sdk.model.request;

import com.hotelbeds.tab.sdk.model.pojo.ActivityFactsheetServiceModalities;
import com.hotelbeds.tab.sdk.model.pojo.PaxDistribution;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;


@XmlRootElement(name="ActivityMultiFactsheetClientRequest")
@XmlType(name = "ActivityMultiFactsheetClientRequest", namespace = "com.hotelbeds.tab.actapi.v3.request.ActivityMultiFactsheetClientRequest", propOrder = { "dateFrom", "dateTo", "codes"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivityMultiFactsheetClientRequest {

    
    private List<ActivityFactsheetServiceModalities> codes;
    
    private String language;


    public ActivityMultiFactsheetClientRequest() {
    	this.codes = new ArrayList<ActivityFactsheetServiceModalities>();
    }

    public ActivityMultiFactsheetClientRequest(List<ActivityFactsheetServiceModalities> codes) {
        super();
        this.codes = codes;
    }

    public List<ActivityFactsheetServiceModalities> getCodes() {
        return codes;
    }

    public void setCodes(List<ActivityFactsheetServiceModalities> codes) {
        this.codes = codes;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Codes[=" + codes).append("],");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.codes);
        hash = 53 * hash + Objects.hashCode(this.language);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActivityMultiFactsheetClientRequest other = (ActivityMultiFactsheetClientRequest) obj;
        if (!Objects.equals(this.codes, other.codes)) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        return true;
    }
    
    

}
