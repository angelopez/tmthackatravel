package  com.hotelbeds.tab.sdk.model.request;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import com.hotelbeds.tab.sdk.model.domain.ActivityOrderType;
import com.hotelbeds.tab.sdk.model.pojo.ActivityFactsheetServiceModalities;
import com.hotelbeds.tab.sdk.model.pojo.ActivitySearchFilterItem;
import com.hotelbeds.tab.sdk.model.pojo.ActivitySearchFilterItemList;
import com.hotelbeds.tab.sdk.model.pojo.PaginationRequest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonFormat;

@XmlRootElement(name = "ActivitySearchRequest")
@XmlType(name = "ActivitySearchRequest",
        namespace = "com.hotelbeds.tab.actapi.v3.request.ActivitySearchRequest",
        propOrder = { "filters", "from", "to", "pagination", "order"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivitySearchRequest implements WithPaginationRequest {

    
    private List<ActivitySearchFilterItemList>   filters;

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date                                 from;

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date                                 to;

    
    private PaginationRequest                    pagination;

    
    private ActivityOrderType                    order;
    
    private String language;

    public ActivitySearchRequest() {
    	this.filters = new ArrayList<ActivitySearchFilterItemList>();
	}
    
    public ActivitySearchRequest(List<ActivitySearchFilterItemList> filters,
			Date from, Date to, PaginationRequest pagination,
			ActivityOrderType order, String language) {
		super();
		this.filters = filters;
		this.from = from;
		this.to = to;
		this.pagination = pagination;
		this.order = order;
		this.language = language;
	}

	public List<ActivitySearchFilterItemList> getFilters() {
        return filters;
    }

    public void setFilters(List<ActivitySearchFilterItemList> filters) {
        this.filters = filters;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public PaginationRequest getPagination() {
        return pagination;
    }

    public void setPagination(PaginationRequest pagination) {
        this.pagination = pagination;
    }

    public ActivityOrderType getOrder() {
        return order;
    }

    public void setOrder(ActivityOrderType order) {
        this.order = order;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    @Override
    public String toString() {
    	//TODO: Falta por determinar los campos que se muestran. Primera Propuesta
    	StringBuilder sb = new StringBuilder();
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	if(from != null) sb.append("From=").append(sdf.format(from)).append(",");
    	if(to != null) sb.append("To=").append(sdf.format(to)).append(",");
		if(filters != null){
			for(ActivitySearchFilterItemList list : filters){
				if(list != null){
					for(ActivitySearchFilterItem item : list.getSearchFilterItems()){
						sb.append("FilterItemType=").append(item.getType()).append(",");
						sb.append("FilterItemValue=").append(item.getValue()).append(",");
					}
				}
			}
		}
    	sb.append("class=ActivitySearchRequest");
    	return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.filters);
        hash = 37 * hash + Objects.hashCode(this.from);
        hash = 37 * hash + Objects.hashCode(this.to);
        hash = 37 * hash + Objects.hashCode(this.pagination);
        hash = 37 * hash + Objects.hashCode(this.order);
        hash = 37 * hash + Objects.hashCode(this.language);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActivitySearchRequest other = (ActivitySearchRequest) obj;
        if (!Objects.equals(this.filters, other.filters)) {
            return false;
        }
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.pagination, other.pagination)) {
            return false;
        }
        if (this.order != other.order) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        return true;
    }
    
    
    
}
