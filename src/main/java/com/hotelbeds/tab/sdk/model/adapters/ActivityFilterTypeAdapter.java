package com.hotelbeds.tab.sdk.model.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.hotelbeds.tab.sdk.model.domain.ActivityFilterType;

public class ActivityFilterTypeAdapter extends XmlAdapter<String, ActivityFilterType> {

    @Override
    public ActivityFilterType unmarshal(String str) throws Exception {
        return ActivityFilterType.fromType(str);
    }

    @Override
    public String marshal(ActivityFilterType obj) throws Exception {
        return obj.getType();
    }

}
