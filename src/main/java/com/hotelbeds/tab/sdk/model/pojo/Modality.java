package com.hotelbeds.tab.sdk.model.pojo;


import com.hotelbeds.tab.sdk.model.domain.ModalityUnitType;
import javax.xml.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@XmlType(name = "Modality")
public class Modality  {

    private Duration                duration;
    private List<QuestionDetail>    questions;
    private List<Comment>           comments;
    private SupplierInformation     supplierInformation;
    private ProviderInformation     providerInformation;
    private String                  destinationCode;
    private Integer                 minChildrenAge;
    private Integer                 maxChildrenAge;
    private List<Promotion>         promotions;
    private List<Language>          languages;
    private List<PaxPrice>          amountsFrom;
    private List<Rate>              rates;
    private ModalityUnitType        amountUnitType;
    private FactsheetActivity specificContent;
    private String           code;
    private String           name;

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public List<QuestionDetail> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDetail> questions) {
        this.questions = questions;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public SupplierInformation getSupplierInformation() {
        return supplierInformation;
    }

    public void setSupplierInformation(SupplierInformation supplierInformation) {
        this.supplierInformation = supplierInformation;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public Integer getMinChildrenAge() {
        return minChildrenAge;
    }

    public void setMinChildrenAge(Integer minChildrenAge) {
        this.minChildrenAge = minChildrenAge;
    }

    public Integer getMaxChildrenAge() {
        return maxChildrenAge;
    }

    public void setMaxChildrenAge(Integer maxChildrenAge) {
        this.maxChildrenAge = maxChildrenAge;
    }

    public ProviderInformation getProviderInformation() {
        return providerInformation;
    }

    public void setProviderInformation(ProviderInformation providerInformation) {
        this.providerInformation = providerInformation;
    }

    public List<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<Promotion> promotions) {
        this.promotions = promotions;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<PaxPrice> getAmountsFrom() {
        return amountsFrom;
    }

    public void setAmountsFrom(List<PaxPrice> amountsFrom) {
        this.amountsFrom = amountsFrom;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }

    public FactsheetActivity getSpecificContent() {
        return specificContent;
    }

    public ModalityUnitType getAmountUnitType() {
        return amountUnitType;
    }

    public void setAmountUnitType(ModalityUnitType amountUnitType) {
        this.amountUnitType = amountUnitType;
    }

    public void setSpecificContent(FactsheetActivity specificContent) {
        this.specificContent = specificContent;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.duration);
        hash = 53 * hash + Objects.hashCode(this.questions);
        hash = 53 * hash + Objects.hashCode(this.comments);
        hash = 53 * hash + Objects.hashCode(this.supplierInformation);
        hash = 53 * hash + Objects.hashCode(this.providerInformation);
        hash = 53 * hash + Objects.hashCode(this.destinationCode);
        hash = 53 * hash + Objects.hashCode(this.minChildrenAge);
        hash = 53 * hash + Objects.hashCode(this.maxChildrenAge);
        hash = 53 * hash + Objects.hashCode(this.promotions);
        hash = 53 * hash + Objects.hashCode(this.languages);
        hash = 53 * hash + Objects.hashCode(this.amountsFrom);
        hash = 53 * hash + Objects.hashCode(this.rates);
        hash = 53 * hash + Objects.hashCode(this.amountUnitType);
        hash = 53 * hash + Objects.hashCode(this.specificContent);
        hash = 53 * hash + Objects.hashCode(this.code);
        hash = 53 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Modality other = (Modality) obj;
        if (!Objects.equals(this.duration, other.duration)) {
            return false;
        }
        if (!Objects.equals(this.questions, other.questions)) {
            return false;
        }
        if (!Objects.equals(this.comments, other.comments)) {
            return false;
        }
        if (!Objects.equals(this.supplierInformation, other.supplierInformation)) {
            return false;
        }
        if (!Objects.equals(this.providerInformation, other.providerInformation)) {
            return false;
        }
        if (!Objects.equals(this.destinationCode, other.destinationCode)) {
            return false;
        }
        if (!Objects.equals(this.minChildrenAge, other.minChildrenAge)) {
            return false;
        }
        if (!Objects.equals(this.maxChildrenAge, other.maxChildrenAge)) {
            return false;
        }
        if (!Objects.equals(this.promotions, other.promotions)) {
            return false;
        }
        if (!Objects.equals(this.languages, other.languages)) {
            return false;
        }
        if (!Objects.equals(this.amountsFrom, other.amountsFrom)) {
            return false;
        }
        if (!Objects.equals(this.rates, other.rates)) {
            return false;
        }
        if (this.amountUnitType != other.amountUnitType) {
            return false;
        }
        if (!Objects.equals(this.specificContent, other.specificContent)) {
            return false;
        }
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    

}
