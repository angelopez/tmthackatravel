package com.hotelbeds.tab.sdk.model.domain;


import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "BookingListFilterType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
public enum BookingListFilterType
{
    CHECKIN ("checkin"),
    CREATION ("creation"),
    CANCELLATION ("date_cancellation");
    
    private String type;
    
    private BookingListFilterType (String type) {
        this.type = type;
    }
        
    public String getType() {
        return type;
    }

    public static BookingListFilterType fromType(String type) {
        for (BookingListFilterType bookingType : values()) {
            if (bookingType.type.equalsIgnoreCase(type)) {
                return bookingType;
            }
        }
        return BookingListFilterType.CHECKIN;
    }
    
    public static String getAllowedTypes() {
        StringBuilder sb = new StringBuilder();
        for(BookingListFilterType filter: values()) {
            if(sb.length() > 0) { sb.append(", "); }
            sb.append(filter.getType());
        }
        return sb.toString();
    }
    
}
