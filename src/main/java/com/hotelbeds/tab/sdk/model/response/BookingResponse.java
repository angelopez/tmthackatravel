package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.Booking;
import java.text.SimpleDateFormat;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "BookingResponse")
@XmlType(name = "BookingResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.BookingResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingResponse extends BaseResponse {

    
    private Booking booking;

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    @Override
    public String toString() {
        //TODO: Determinar los campos necesario. Propuesta inicial
        StringBuilder sb = new StringBuilder(super.toString()); // auditData
        if (booking != null) {
            sb.append("Referenece=" + booking.getReference()).append(",");
            sb.append("Status=" + booking.getStatus()).append(",");
            sb.append("ClientrReference=" + booking.getClientReference()).append(",");
//            if (booking.getAgency() != null) {
//                sb.append("AgencyName=" + booking.getAgency().getName()).append(",");
//                sb.append("AgencyBranch=" + booking.getAgency().getBranchName()).append(",");
//            }
            sb.append("CreationUser=" + booking.getCreationUser()).append(",");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            if (booking.getCreationDate() != null) {
                sb.append("CreationDate=" + sdf.format(booking.getCreationDate())).append(",");
            }
//            if (booking.getCancellationDate() != null) {
//                sb.append("CancellationDate=" + sdf.format(booking.getCancellationDate())).append(",");
//            }
        }
        sb.append("class=BookingResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.booking);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final BookingResponse other = (BookingResponse) obj;
        if (!Objects.equals(this.booking, other.booking)) {
            return false;
        }
        return true;
    }
    
    

}
