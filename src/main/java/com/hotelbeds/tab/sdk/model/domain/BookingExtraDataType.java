package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "BookingExtraDataType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
public enum BookingExtraDataType
{
    WEBUSER,
    WEBID,
    WEBIP,
    WEBDOMAIN,
    
    INFO_TTOO_BEARING_AMOUNT,
    INFO_TTOO_BEARING_AMOUNT_CURRENCY,
    INFO_TTOO_BEARING_AMOUNT_REASON,
    INFO_TTOO_SERVICE_AMOUNT,
    
    PARAM_PURCHASE_FISCAL_CODE
}
