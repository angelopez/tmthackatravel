package com.hotelbeds.tab.sdk.model.request;

import com.hotelbeds.tab.sdk.model.pojo.ServiceAddDetail;
import java.util.Objects;

import javax.xml.bind.annotation.*;

@XmlType(name = "BookingCreditModificationAddRequest")
public class BookingCreditModificationAddRequest extends AbstractBookingModificationRequest {

    private ServiceAddDetail serviceToAdd;

    public BookingCreditModificationAddRequest() {
	}
    
    public BookingCreditModificationAddRequest(ServiceAddDetail serviceToAdd) {
		super();
		this.serviceToAdd = serviceToAdd;
	}

	public ServiceAddDetail getServiceToAdd() {
        return serviceToAdd;
    }

    public void setServiceToAdd(ServiceAddDetail serviceToAdd) {
        this.serviceToAdd = serviceToAdd;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.serviceToAdd);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final BookingCreditModificationAddRequest other = (BookingCreditModificationAddRequest) obj;
        if (!Objects.equals(this.serviceToAdd, other.serviceToAdd)) {
            return false;
        }
        return true;
    }
    
    

}
