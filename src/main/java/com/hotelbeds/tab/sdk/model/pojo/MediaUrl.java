package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.ImageSize;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "MediaUrl", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages", propOrder = { "dpi", "height", "width", "resource", "sizeType"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class MediaUrl {

    private Integer dpi;
    private Integer height;
    private Integer width;
    private String  resource;
    private ImageSize sizeType;

    public MediaUrl() {
    }

    public MediaUrl(Integer dpi, Integer height, Integer width, String resource) {
        super();
        this.dpi = dpi;
        this.height = height;
        this.width = width;
        this.resource = resource;
    }

    public Integer getDpi() {
        return dpi;
    }

    public void setDpi(Integer dpi) {
        this.dpi = dpi;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public ImageSize getSizeType() {
        return sizeType;
    }

    public void setSizeType(ImageSize sizeType) {
        this.sizeType = sizeType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Dpi=").append(dpi).append(",");
        sb.append("Height=").append(height).append(",");
        sb.append("Width=").append(width).append(",");
        sb.append("Resource=").append(resource).append(",");
        sb.append("ImageSize=" + sizeType).append(",");

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.dpi);
        hash = 83 * hash + Objects.hashCode(this.height);
        hash = 83 * hash + Objects.hashCode(this.width);
        hash = 83 * hash + Objects.hashCode(this.resource);
        hash = 83 * hash + Objects.hashCode(this.sizeType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MediaUrl other = (MediaUrl) obj;
        if (!Objects.equals(this.dpi, other.dpi)) {
            return false;
        }
        if (!Objects.equals(this.height, other.height)) {
            return false;
        }
        if (!Objects.equals(this.width, other.width)) {
            return false;
        }
        if (!Objects.equals(this.resource, other.resource)) {
            return false;
        }
        if (this.sizeType != other.sizeType) {
            return false;
        }
        return true;
    }
    
    

}
