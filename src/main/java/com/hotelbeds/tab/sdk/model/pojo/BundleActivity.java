package com.hotelbeds.tab.sdk.model.pojo;


import java.util.Objects;
import javax.xml.bind.annotation.*;

/**
 * Created by xbibiloni on 27/03/16.
 */
@XmlType(name = "BundleActivity", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "code", "name", "modality" } )
@XmlAccessorType(XmlAccessType.FIELD)
public class BundleActivity {

    
    private String      code;

    
    private String      name;

    
    private DuoTypeName modality;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DuoTypeName getModality() {
        return modality;
    }

    public void setModality(DuoTypeName modality) {
        this.modality = modality;
    }

    @Override
    public String toString() {
        return "BundleActivity{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", modality=" + modality +
                '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.code);
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.modality);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BundleActivity other = (BundleActivity) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.modality, other.modality)) {
            return false;
        }
        return true;
    }
    
    
}
