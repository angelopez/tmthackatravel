package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlType(name = "Seat",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = {"customerId", "entranceDoor", "gate", "row", "seat"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Seat {

    
    private String customerId;

    
    private String entranceDoor;

    
    private String gate;

    
    private String row;

    
    private String seat;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getEntranceDoor() {
        return entranceDoor;
    }

    public void setEntranceDoor(String entranceDoor) {
        this.entranceDoor = entranceDoor;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.customerId);
        hash = 41 * hash + Objects.hashCode(this.entranceDoor);
        hash = 41 * hash + Objects.hashCode(this.gate);
        hash = 41 * hash + Objects.hashCode(this.row);
        hash = 41 * hash + Objects.hashCode(this.seat);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Seat other = (Seat) obj;
        if (!Objects.equals(this.customerId, other.customerId)) {
            return false;
        }
        if (!Objects.equals(this.entranceDoor, other.entranceDoor)) {
            return false;
        }
        if (!Objects.equals(this.gate, other.gate)) {
            return false;
        }
        if (!Objects.equals(this.row, other.row)) {
            return false;
        }
        if (!Objects.equals(this.seat, other.seat)) {
            return false;
        }
        return true;
    }

}
