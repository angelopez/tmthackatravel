package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ServiceAddDetail")
public class ServiceAddDetail extends BookingConfirmService {

    private static final long serialVersionUID = 1L;
    private Long hotelCode;

    public Long getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(Long hotelCode) {
        this.hotelCode = hotelCode;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.hotelCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final ServiceAddDetail other = (ServiceAddDetail) obj;
        if (!Objects.equals(this.hotelCode, other.hotelCode)) {
            return false;
        }
        return true;
    }

}
