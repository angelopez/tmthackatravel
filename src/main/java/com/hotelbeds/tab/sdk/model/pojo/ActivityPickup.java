package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "ActivityPickup")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivityPickup {

    private String rateKey;
    private String pickupName;
    private String zoneCode;
    private String zoneName;
    private String shortDescription;
    private String description;
    private Coordinate geolocation;
    private String time;
    private String hotelName;
    private List<PaxPrice> amountsFrom;
    private PickupOperationDate operationDate;
    private String modalityCode;
    private Long officeCode;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date validFrom;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date validTo;
    private Boolean validMonday;
    private Boolean validTuesday;
    private Boolean validWednesday;
    private Boolean validThursday;
    private Boolean validFriday;
    private Boolean validSaturday;
    private Boolean validSunday;
    private String pickupPointCode;
    private Boolean generic;
    private Long hotelCode;

    public String getRateKey() {
        return rateKey;
    }

    public void setRateKey(String rateKey) {
        this.rateKey = rateKey;
    }

    public String getPickupName() {
        return pickupName;
    }

    public void setPickupName(String pickupName) {
        this.pickupName = pickupName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Coordinate getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Coordinate geolocation) {
        this.geolocation = geolocation;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public List<PaxPrice> getAmountsFrom() {
        return amountsFrom;
    }

    public void setAmountsFrom(List<PaxPrice> amountsFrom) {
        this.amountsFrom = amountsFrom;
    }

    public PickupOperationDate getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(PickupOperationDate operationDate) {
        this.operationDate = operationDate;
    }

    public String getZoneCode() {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getModalityCode() {
        return modalityCode;
    }

    public void setModalityCode(String modalityCode) {
        this.modalityCode = modalityCode;
    }

    public Long getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(Long officeCode) {
        this.officeCode = officeCode;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Boolean isValidMonday() {
        return validMonday;
    }

    public void setValidMonday(Boolean validMonday) {
        this.validMonday = validMonday;
    }

    public Boolean isValidTuesday() {
        return validTuesday;
    }

    public void setValidTuesday(Boolean validTuesday) {
        this.validTuesday = validTuesday;
    }

    public Boolean isValidWednesday() {
        return validWednesday;
    }

    public void setValidWednesday(Boolean validWednesday) {
        this.validWednesday = validWednesday;
    }

    public Boolean isValidThursday() {
        return validThursday;
    }

    public void setValidThursday(Boolean validThursday) {
        this.validThursday = validThursday;
    }

    public Boolean isValidFriday() {
        return validFriday;
    }

    public void setValidFriday(Boolean validFriday) {
        this.validFriday = validFriday;
    }

    public Boolean isValidSaturday() {
        return validSaturday;
    }

    public void setValidSaturday(Boolean validSaturday) {
        this.validSaturday = validSaturday;
    }

    public Boolean isValidSunday() {
        return validSunday;
    }

    public void setValidSunday(Boolean validSunday) {
        this.validSunday = validSunday;
    }

    public String getPickupPointCode() {
        return pickupPointCode;
    }

    public void setPickupPointCode(String pickupPointCode) {
        this.pickupPointCode = pickupPointCode;
    }

    public Boolean getGeneric() {
        return generic;
    }

    public void setGeneric(Boolean generic) {
        this.generic = generic;
    }

    public Long getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(Long hotelCode) {
        this.hotelCode = hotelCode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.rateKey);
        hash = 59 * hash + Objects.hashCode(this.pickupName);
        hash = 59 * hash + Objects.hashCode(this.zoneCode);
        hash = 59 * hash + Objects.hashCode(this.zoneName);
        hash = 59 * hash + Objects.hashCode(this.shortDescription);
        hash = 59 * hash + Objects.hashCode(this.description);
        hash = 59 * hash + Objects.hashCode(this.geolocation);
        hash = 59 * hash + Objects.hashCode(this.time);
        hash = 59 * hash + Objects.hashCode(this.hotelName);
        hash = 59 * hash + Objects.hashCode(this.amountsFrom);
        hash = 59 * hash + Objects.hashCode(this.operationDate);
        hash = 59 * hash + Objects.hashCode(this.modalityCode);
        hash = 59 * hash + Objects.hashCode(this.officeCode);
        hash = 59 * hash + Objects.hashCode(this.validFrom);
        hash = 59 * hash + Objects.hashCode(this.validTo);
        hash = 59 * hash + Objects.hashCode(this.validMonday);
        hash = 59 * hash + Objects.hashCode(this.validTuesday);
        hash = 59 * hash + Objects.hashCode(this.validWednesday);
        hash = 59 * hash + Objects.hashCode(this.validThursday);
        hash = 59 * hash + Objects.hashCode(this.validFriday);
        hash = 59 * hash + Objects.hashCode(this.validSaturday);
        hash = 59 * hash + Objects.hashCode(this.validSunday);
        hash = 59 * hash + Objects.hashCode(this.pickupPointCode);
        hash = 59 * hash + Objects.hashCode(this.generic);
        hash = 59 * hash + Objects.hashCode(this.hotelCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActivityPickup other = (ActivityPickup) obj;
        if (!Objects.equals(this.rateKey, other.rateKey)) {
            return false;
        }
        if (!Objects.equals(this.pickupName, other.pickupName)) {
            return false;
        }
        if (!Objects.equals(this.zoneCode, other.zoneCode)) {
            return false;
        }
        if (!Objects.equals(this.zoneName, other.zoneName)) {
            return false;
        }
        if (!Objects.equals(this.shortDescription, other.shortDescription)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.geolocation, other.geolocation)) {
            return false;
        }
        if (!Objects.equals(this.time, other.time)) {
            return false;
        }
        if (!Objects.equals(this.hotelName, other.hotelName)) {
            return false;
        }
        if (!Objects.equals(this.amountsFrom, other.amountsFrom)) {
            return false;
        }
        if (!Objects.equals(this.operationDate, other.operationDate)) {
            return false;
        }
        if (!Objects.equals(this.modalityCode, other.modalityCode)) {
            return false;
        }
        if (!Objects.equals(this.officeCode, other.officeCode)) {
            return false;
        }
        if (!Objects.equals(this.validFrom, other.validFrom)) {
            return false;
        }
        if (!Objects.equals(this.validTo, other.validTo)) {
            return false;
        }
        if (!Objects.equals(this.validMonday, other.validMonday)) {
            return false;
        }
        if (!Objects.equals(this.validTuesday, other.validTuesday)) {
            return false;
        }
        if (!Objects.equals(this.validWednesday, other.validWednesday)) {
            return false;
        }
        if (!Objects.equals(this.validThursday, other.validThursday)) {
            return false;
        }
        if (!Objects.equals(this.validFriday, other.validFriday)) {
            return false;
        }
        if (!Objects.equals(this.validSaturday, other.validSaturday)) {
            return false;
        }
        if (!Objects.equals(this.validSunday, other.validSunday)) {
            return false;
        }
        if (!Objects.equals(this.pickupPointCode, other.pickupPointCode)) {
            return false;
        }
        if (!Objects.equals(this.generic, other.generic)) {
            return false;
        }
        if (!Objects.equals(this.hotelCode, other.hotelCode)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
