package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "ReedemType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum ReedemType
{
    EVOUCHER("EVOUCHER"),
    PRINTED("PRINTED"),
    VOCUHERLESS("VOCUHERLESS"),
    NONE("NONE");
    
    private String code;

    ReedemType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static ReedemType fromCode(String code) {
        for (ReedemType type : ReedemType.values()) {
            if (type.getCode().equalsIgnoreCase(code)) {
                return type;
            }
        }
        return NONE;
    }
}
