package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.PointOfInterestType;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;


@XmlType(name = "PointOfInterest")
@XmlAccessorType(XmlAccessType.FIELD)
public class PointOfInterest {

    
    private PointOfInterestType type;
    
    private Coordinate          geolocation;
    
    private String              address;
    
    private Country             country;
    
    private String              city;
    
    private List<Area>          area;
    
    private String              zip;
    
    private String              description;

    public PointOfInterest() {
    }

    public PointOfInterest(PointOfInterestType type, Coordinate geolocation, String address, Country country, String city, List<Area> area, String zip, String description) {
        this.type = type;
        this.geolocation = geolocation;
        this.address = address;
        this.country = country;
        this.city = city;
        this.area = area;
        this.zip = zip;
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Coordinate getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Coordinate geolocation) {
        this.geolocation = geolocation;
    }

    public PointOfInterestType getType() {
        return type;
    }

    public void setType(PointOfInterestType type) {
        this.type = type;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public List<Area> getArea() {
        return area;
    }

    public void setArea(List<Area> area) {
        this.area = area;
    }
    
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Address=").append(address).append(",");
        sb.append("City=").append(city).append(",");
        sb.append("Country=").append(country).append(",");
        sb.append("Description=").append(description).append(",");
        sb.append("Geolocation=").append(geolocation).append(",");
        sb.append("Type=").append(type).append(",");
        sb.append("Zip=").append(zip).append(",");
        sb.append("Area=").append(area);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.type);
        hash = 37 * hash + Objects.hashCode(this.geolocation);
        hash = 37 * hash + Objects.hashCode(this.address);
        hash = 37 * hash + Objects.hashCode(this.country);
        hash = 37 * hash + Objects.hashCode(this.city);
        hash = 37 * hash + Objects.hashCode(this.area);
        hash = 37 * hash + Objects.hashCode(this.zip);
        hash = 37 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointOfInterest other = (PointOfInterest) obj;
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.geolocation, other.geolocation)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.area, other.area)) {
            return false;
        }
        if (!Objects.equals(this.zip, other.zip)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return true;
    }
    
    

}
