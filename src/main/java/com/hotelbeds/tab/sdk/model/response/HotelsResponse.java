package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.Hotel;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "HotelsResponse")
@XmlType(name = "HotelsResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.HotelsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class HotelsResponse extends BaseResponse {

    
    private List<Hotel> hotels;

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }

    @Override
    public String toString() {
        //TODO: Falta por determinar los atributos que se muestran. Primera propuesta
        StringBuilder sb = new StringBuilder(super.toString());
        if (hotels != null) {
            sb.append("NumOfHotels=" + hotels.size()).append(",");
        } else {
            sb.append("NumOfHotels=0").append(",");
        }
        sb.append("class=HotelsResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.hotels);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final HotelsResponse other = (HotelsResponse) obj;
        if (!Objects.equals(this.hotels, other.hotels)) {
            return false;
        }
        return true;
    }
    
    
    
}
