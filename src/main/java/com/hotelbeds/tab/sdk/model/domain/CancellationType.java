package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "CancellationType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
public enum CancellationType {

	SIMULATION,
	CANCELLATION
}
