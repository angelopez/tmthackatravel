package com.hotelbeds.tab.sdk.model.pojo;

import java.util.List;
import java.util.Objects;
import java.util.Random;

import javax.xml.bind.annotation.XmlType;

import com.hotelbeds.tab.sdk.model.domain.ActivityType;

@XmlType(name = "Activity")
public class Activity {

    private String url;
    private List<Promotion> promotions;
    private List<SearchModality> modalities;
    private FactsheetActivity content;
    private Integer order;
    private List<PaxPrice> amountsFrom;
    private String currencyName;
    private Country country;
    private String name;
    private String currency;
    private String code;
    private String contentId;
    private ActivityType type;
    private int paxes;
    private int freeSlots;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<Promotion> promotions) {
        this.promotions = promotions;
    }

    public List<SearchModality> getModalities() {
        return modalities;
    }

    public void setModalities(List<SearchModality> modalities) {
        this.modalities = modalities;
    }

    public FactsheetActivity getContent() {
        return content;
    }

    public void setContent(FactsheetActivity content) {
        this.content = content;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<PaxPrice> getAmountsFrom() {
        return amountsFrom;
    }

    public void setAmountsFrom(List<PaxPrice> amountsFrom) {
        this.amountsFrom = amountsFrom;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public ActivityType getType() {
        return type;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }

    public void setPaxes(int paxes) {
        this.paxes = paxes;
    }

    public int getPaxes() {
        return paxes;
    }

    public void setFreeSlots(int freeSlots) {
        this.freeSlots = freeSlots;
    }


    public int getFreeSlots() {
        return freeSlots;
    }

    public Activity createNew(Segment segment) {
        Activity activity = new Activity();
        activity.setUrl(this.url);
        activity.setPromotions(this.promotions);
        activity.setModalities(this.modalities);
        activity.setContent(this.content);
        activity.setOrder(this.order);
        activity.setAmountsFrom(this.amountsFrom);
        activity.setCurrencyName(this.currencyName);
        activity.setCountry(this.country);
        activity.setName(this.name);
        activity.setCurrency(this.currency);
        activity.setCode(this.code);
        activity.setContentId(contentId);
        activity.setType(this.type);
        Random random1 = new Random();
        int random = random1.nextInt(15) + 5;
        activity.setPaxes(random);
        activity.setFreeSlots(random1.nextInt(random));
        return activity;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.url);
        hash = 67 * hash + Objects.hashCode(this.promotions);
        hash = 67 * hash + Objects.hashCode(this.modalities);
        hash = 67 * hash + Objects.hashCode(this.content);
        hash = 67 * hash + Objects.hashCode(this.order);
        hash = 67 * hash + Objects.hashCode(this.amountsFrom);
        hash = 67 * hash + Objects.hashCode(this.currencyName);
        hash = 67 * hash + Objects.hashCode(this.country);
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + Objects.hashCode(this.currency);
        hash = 67 * hash + Objects.hashCode(this.code);
        hash = 67 * hash + Objects.hashCode(this.contentId);
        hash = 67 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Activity other = (Activity) obj;
        if (!Objects.equals(this.url, other.url)) {
            return false;
        }
        if (!Objects.equals(this.promotions, other.promotions)) {
            return false;
        }
        if (!Objects.equals(this.modalities, other.modalities)) {
            return false;
        }
        if (!Objects.equals(this.content, other.content)) {
            return false;
        }
        if (!Objects.equals(this.order, other.order)) {
            return false;
        }
        if (!Objects.equals(this.amountsFrom, other.amountsFrom)) {
            return false;
        }
        if (!Objects.equals(this.currencyName, other.currencyName)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.contentId, other.contentId)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }



}
