package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.Country;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "DestinationsResponse")
@XmlType(name = "DestinationsResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.DestinationsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class DestinationsResponse extends BaseResponse {

    
    private Country country;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        //TODO: Falta por determinar los atributos que se muestran. Primera propuesta
        StringBuilder sb = new StringBuilder(super.toString());
        if (country != null && country.getDestinations() != null) {
            sb.append("NumOfDestinations=" + country.getDestinations().size()).append(",");
        } else {
            sb.append("NumOfDestinations=0").append(",");
        }
        sb.append("class=DestinationsResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.country);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final DestinationsResponse other = (DestinationsResponse) obj;
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        return true;
    }
    
    
}
