package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "CommentType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlEnum
public enum CommentType
{

    CONTRACT_REMARKS,
    VOUCHER,
    OFFICE_REMARKS,
    UNKNOWN;

    public static CommentType fromString(String str) {
        try {
            return CommentType.valueOf(str);
        } catch (IllegalArgumentException iae) {
            return CommentType.UNKNOWN;
        }
    }

}
