package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.Activity;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "ActivitySearchResponse")
@XmlType(name = "ActivitySearchResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.ActivitySearchResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivitySearchResponse extends BaseResponse {

    
    private List<Activity> activities;

    public List<Activity> getActivities() {
        return activities;
    }
    
    private PaginationResponse pagination;

    public PaginationResponse getPagination() {
        return pagination;
    }

    public void setPagination(PaginationResponse pagination) {
        this.pagination = pagination;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public void addAllActivities(List<Activity> activities) {
        if (this.getActivities() == null) {
            this.setActivities(new ArrayList<Activity>());
        }
        this.getActivities().addAll(activities);
    }

    @Override
    public String toString() {
        //TODO: Por determinar los campos a mostar. Primera propuesta
        StringBuilder sb = new StringBuilder(super.toString()); // auditData
        if (activities != null) {
            sb.append("NumOfActivities=").append(activities.size()).append(",");
        } else {
            sb.append("NumOfActivities=0").append(",");
        }
        sb.append("class=ActivitySearchResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.activities);
        hash = 43 * hash + Objects.hashCode(this.pagination);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final ActivitySearchResponse other = (ActivitySearchResponse) obj;
        if (!Objects.equals(this.activities, other.activities)) {
            return false;
        }
        if (!Objects.equals(this.pagination, other.pagination)) {
            return false;
        }
        return true;
    }
    
    

}
