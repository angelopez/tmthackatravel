package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingTransaction",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = {"transactionReference",
            "paymentType",
            "amount",
            "charge",
            "currency",
            "maskedCardNumber",
            "authorizationCode",
            "paymentResponse",
            "remarks"})
public class BookingTransaction {

    
    private String transactionReference;

    
    private String paymentResponse;

    
    private String paymentType;

    
    private BigDecimal amount;

    
    private BigDecimal charge;

    
    private String authorizationCode;

    
    private String currency;

    
    private String maskedCardNumber;

    
    private String remarks;

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getPaymentResponse() {
        return paymentResponse;
    }

    public void setPaymentResponse(String paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getCharge() {
        return charge;
    }

    public void setCharge(BigDecimal charge) {
        this.charge = charge;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    public void setMaskedCardNumber(String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.transactionReference);
        hash = 11 * hash + Objects.hashCode(this.paymentResponse);
        hash = 11 * hash + Objects.hashCode(this.paymentType);
        hash = 11 * hash + Objects.hashCode(this.amount);
        hash = 11 * hash + Objects.hashCode(this.charge);
        hash = 11 * hash + Objects.hashCode(this.authorizationCode);
        hash = 11 * hash + Objects.hashCode(this.currency);
        hash = 11 * hash + Objects.hashCode(this.maskedCardNumber);
        hash = 11 * hash + Objects.hashCode(this.remarks);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookingTransaction other = (BookingTransaction) obj;
        if (!Objects.equals(this.transactionReference, other.transactionReference)) {
            return false;
        }
        if (!Objects.equals(this.paymentResponse, other.paymentResponse)) {
            return false;
        }
        if (!Objects.equals(this.paymentType, other.paymentType)) {
            return false;
        }
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        if (!Objects.equals(this.charge, other.charge)) {
            return false;
        }
        if (!Objects.equals(this.authorizationCode, other.authorizationCode)) {
            return false;
        }
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        if (!Objects.equals(this.maskedCardNumber, other.maskedCardNumber)) {
            return false;
        }
        if (!Objects.equals(this.remarks, other.remarks)) {
            return false;
        }
        return true;
    }

}
