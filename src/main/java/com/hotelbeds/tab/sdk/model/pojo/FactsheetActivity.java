package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import com.hotelbeds.tab.sdk.model.domain.ActivityFactsheetType;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "FactsheetActivity")
@XmlAccessorType(XmlAccessType.FIELD)
public class FactsheetActivity {

    private ActivityFactsheetType activityFactsheetType;
    private String activityCode;
    private String modalityCode;
    private String contentId;
    private String description;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date lastUpdate;
    private String summary;
    private List<String> advancedTips;
    private List<Country> countries;
    private List<String> highligths;
    private String name;
    private List<String> detailedInfo;
    private List<FeatureGroup> featureGroups;
    private GuidingOptions guidingOptions;
    private List<String> importantInfo; //suma
    private Location location;
    private Media media; //suma
    private List<Note> notes;
    private Redeem redeemInfo;
    private List<Route> routes;
    private Schedule scheduling;
    private List<SegmentGroup> segmentationGroups;
    private Coordinate geolocation;
    private List<Venue> venueIds;

    public FactsheetActivity() {
    }

    public ActivityFactsheetType getActivityFactsheetType() {
        return activityFactsheetType;
    }

    public void setActivityFactsheetType(ActivityFactsheetType activityFactsheetType) {
        this.activityFactsheetType = activityFactsheetType;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getModalityCode() {
        return modalityCode;
    }

    public void setModalityCode(String modalityCode) {
        this.modalityCode = modalityCode;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<String> getAdvancedTips() {
        return advancedTips;
    }

    public void setAdvancedTips(List<String> advancedTips) {
        this.advancedTips = advancedTips;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public List<String> getHighligths() {
        return highligths;
    }

    public void setHighligths(List<String> highligths) {
        this.highligths = highligths;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getDetailedInfo() {
        return detailedInfo;
    }

    public void setDetailedInfo(List<String> detailedInfo) {
        this.detailedInfo = detailedInfo;
    }

    public List<FeatureGroup> getFeatureGroups() {
        return featureGroups;
    }

    public void setFeatureGroups(List<FeatureGroup> featureGroups) {
        this.featureGroups = featureGroups;
    }

    public GuidingOptions getGuidingOptions() {
        return guidingOptions;
    }

    public void setGuidingOptions(GuidingOptions guidingOptions) {
        this.guidingOptions = guidingOptions;
    }

    public List<String> getImportantInfo() {
        return importantInfo;
    }

    public void setImportantInfo(List<String> importantInfo) {
        this.importantInfo = importantInfo;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public Redeem getRedeemInfo() {
        return redeemInfo;
    }

    public void setRedeemInfo(Redeem redeemInfo) {
        this.redeemInfo = redeemInfo;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public Schedule getScheduling() {
        return scheduling;
    }

    public void setScheduling(Schedule scheduling) {
        this.scheduling = scheduling;
    }

    public List<SegmentGroup> getSegmentationGroups() {
        return segmentationGroups;
    }

    public void setSegmentationGroups(List<SegmentGroup> segmentationGroups) {
        this.segmentationGroups = segmentationGroups;
    }

    public Coordinate getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Coordinate geolocation) {
        this.geolocation = geolocation;
    }

    public List<Venue> getVenueIds() {
        return venueIds;
    }

    public void setVenueIds(List<Venue> venueIds) {
        this.venueIds = venueIds;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.activityFactsheetType);
        hash = 17 * hash + Objects.hashCode(this.activityCode);
        hash = 17 * hash + Objects.hashCode(this.modalityCode);
        hash = 17 * hash + Objects.hashCode(this.contentId);
        hash = 17 * hash + Objects.hashCode(this.description);
        hash = 17 * hash + Objects.hashCode(this.lastUpdate);
        hash = 17 * hash + Objects.hashCode(this.summary);
        hash = 17 * hash + Objects.hashCode(this.advancedTips);
        hash = 17 * hash + Objects.hashCode(this.countries);
        hash = 17 * hash + Objects.hashCode(this.highligths);
        hash = 17 * hash + Objects.hashCode(this.name);
        hash = 17 * hash + Objects.hashCode(this.detailedInfo);
        hash = 17 * hash + Objects.hashCode(this.featureGroups);
        hash = 17 * hash + Objects.hashCode(this.guidingOptions);
        hash = 17 * hash + Objects.hashCode(this.importantInfo);
        hash = 17 * hash + Objects.hashCode(this.location);
        hash = 17 * hash + Objects.hashCode(this.media);
        hash = 17 * hash + Objects.hashCode(this.notes);
        hash = 17 * hash + Objects.hashCode(this.redeemInfo);
        hash = 17 * hash + Objects.hashCode(this.routes);
        hash = 17 * hash + Objects.hashCode(this.scheduling);
        hash = 17 * hash + Objects.hashCode(this.segmentationGroups);
        hash = 17 * hash + Objects.hashCode(this.geolocation);
        hash = 17 * hash + Objects.hashCode(this.venueIds);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FactsheetActivity other = (FactsheetActivity) obj;
        if (this.activityFactsheetType != other.activityFactsheetType) {
            return false;
        }
        if (!Objects.equals(this.activityCode, other.activityCode)) {
            return false;
        }
        if (!Objects.equals(this.modalityCode, other.modalityCode)) {
            return false;
        }
        if (!Objects.equals(this.contentId, other.contentId)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.lastUpdate, other.lastUpdate)) {
            return false;
        }
        if (!Objects.equals(this.summary, other.summary)) {
            return false;
        }
        if (!Objects.equals(this.advancedTips, other.advancedTips)) {
            return false;
        }
        if (!Objects.equals(this.countries, other.countries)) {
            return false;
        }
        if (!Objects.equals(this.highligths, other.highligths)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.detailedInfo, other.detailedInfo)) {
            return false;
        }
        if (!Objects.equals(this.featureGroups, other.featureGroups)) {
            return false;
        }
        if (!Objects.equals(this.guidingOptions, other.guidingOptions)) {
            return false;
        }
        if (!Objects.equals(this.importantInfo, other.importantInfo)) {
            return false;
        }
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        if (!Objects.equals(this.media, other.media)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.redeemInfo, other.redeemInfo)) {
            return false;
        }
        if (!Objects.equals(this.routes, other.routes)) {
            return false;
        }
        if (!Objects.equals(this.scheduling, other.scheduling)) {
            return false;
        }
        if (!Objects.equals(this.segmentationGroups, other.segmentationGroups)) {
            return false;
        }
        if (!Objects.equals(this.geolocation, other.geolocation)) {
            return false;
        }
        if (!Objects.equals(this.venueIds, other.venueIds)) {
            return false;
        }
        return true;
    }
    

}
