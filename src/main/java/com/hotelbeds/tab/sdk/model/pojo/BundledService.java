package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.ActivityType;
import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@XmlType(name = "BundledService")
@XmlAccessorType(XmlAccessType.FIELD)
public class BundledService {

    
    private Integer order;

    
    private BundleActivity activity;

    
    private ActivityType type;

    
    private String payableName;

    
    private List<Comment> comments;

    
    private ProviderInformation providerInformation;

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public BundleActivity getActivity() {
        return activity;
    }

    public void setActivity(BundleActivity activity) {
        this.activity = activity;
    }

    public ActivityType getType() {
        return type;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }

    public String getPayableName() {
        return payableName;
    }

    public void setPayableName(String payableName) {
        this.payableName = payableName;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public ProviderInformation getProviderInformation() {
        return providerInformation;
    }

    public void setProviderInformation(ProviderInformation providerInformation) {
        this.providerInformation = providerInformation;
    }

    @Override
    public String toString() {
        return "BundledService{"
                + "order=" + order
                + ", activity=" + activity
                + ", type=" + type
                + ", payableName='" + payableName + '\''
                + ", comments=" + comments
                + ", providerInformation=" + providerInformation
                + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.order);
        hash = 11 * hash + Objects.hashCode(this.activity);
        hash = 11 * hash + Objects.hashCode(this.type);
        hash = 11 * hash + Objects.hashCode(this.payableName);
        hash = 11 * hash + Objects.hashCode(this.comments);
        hash = 11 * hash + Objects.hashCode(this.providerInformation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BundledService other = (BundledService) obj;
        if (!Objects.equals(this.order, other.order)) {
            return false;
        }
        if (!Objects.equals(this.activity, other.activity)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.payableName, other.payableName)) {
            return false;
        }
        if (!Objects.equals(this.comments, other.comments)) {
            return false;
        }
        if (!Objects.equals(this.providerInformation, other.providerInformation)) {
            return false;
        }
        return true;
    }

}
