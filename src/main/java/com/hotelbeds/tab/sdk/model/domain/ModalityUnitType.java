package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ModalityUnitType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlEnum
public enum ModalityUnitType {

	PAX("P"),
	SERVICE("S"),
	UNKNOWN("?");
	
	private String atlasType;
	
	private ModalityUnitType(String atlasType) {
		this.atlasType = atlasType;
	}
	
	public String getAtlasType() {
		return this.atlasType;
	}
	
	public static ModalityUnitType fromATLAS(String atlas) {
		for(ModalityUnitType type: ModalityUnitType.values()) {
			if(type.atlasType.equalsIgnoreCase(atlas)) {
				return type;
			}
		}
		return UNKNOWN;
	}
	
}
