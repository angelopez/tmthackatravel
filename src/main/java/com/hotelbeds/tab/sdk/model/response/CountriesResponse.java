package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.Country;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "CountriesResponse")
@XmlType(name = "CountriesResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.CountriesResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CountriesResponse extends BaseResponse {

    
    private List<Country> countries;

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @Override
    public String toString() {
        //TODO: Falta por determinar los atributos que se muestran. Primera propuesta
        StringBuilder sb = new StringBuilder(super.toString()); // auditData
        if (countries != null) {
            sb.append("NumOfCountries=" + countries.size()).append(",");
        } else {
            sb.append("NumOfCountries=0").append(",");
        }
        sb.append("class=CountriesResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.countries);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final CountriesResponse other = (CountriesResponse) obj;
        if (!Objects.equals(this.countries, other.countries)) {
            return false;
        }
        return true;
    }
    
    
}
