package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;

@XmlType(name = "CancellationPolicy",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "dateFrom", "amount"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class CancellationPolicy implements Comparable {

    
    private String     dateFrom;
    
    private BigDecimal amount;

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof CancellationPolicy)) {
            return false;
        }
        CancellationPolicy fee = (CancellationPolicy) o;
        if(this.getDateFrom() != null && fee.getDateFrom() == null || this.getDateFrom() == null && fee.getDateFrom() != null || (!this.getDateFrom().equals(fee.getDateFrom()))) {
            return false;
        }
        if(this.getAmount() != null && fee.getAmount() == null || this.getAmount() == null && fee.getAmount() != null || (this.getAmount().doubleValue() != fee.getAmount().doubleValue())) {
            return false;
        }
        return true;
    }
    
    @Override
    public int compareTo(Object o) {
        if(!(o instanceof CancellationPolicy)) {
            return -1;
        }
        CancellationPolicy fee = (CancellationPolicy) o;
        if(this.getDateFrom() != null && fee.getDateFrom() != null) {
            return this.getDateFrom().compareTo(fee.getDateFrom());
        } else if(this.getDateFrom() != null) {
            return -1;
        } else {
            return 1;
        }
    }
    
}
