package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.ReedemType;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;

@XmlType(name = "Redeem",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = {"type", "directEntrance", "comments"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Redeem {

    
    private ReedemType type;
    
    private boolean directEntrance;
    
    private List<DuoTypeDescription> comments;

    public Redeem() {
    }

    public Redeem(boolean directEntrance, ReedemType type, List<DuoTypeDescription> comments) {
        super();
        this.directEntrance = directEntrance;
        this.type = type;
        this.comments = comments;
    }

    public boolean isDirectEntrance() {
        return directEntrance;
    }

    public void setDirectEntrance(boolean directEntrance) {
        this.directEntrance = directEntrance;
    }

    public ReedemType getType() {
        return type;
    }

    public void setType(ReedemType type) {
        this.type = type;
    }

    public List<DuoTypeDescription> getComments() {
        return comments;
    }

    public void setComments(List<DuoTypeDescription> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DirectEntrance=").append(directEntrance).append(",");
        sb.append("Type=").append(type).append(",");
        sb.append("Commments=").append(comments);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.type);
        hash = 53 * hash + (this.directEntrance ? 1 : 0);
        hash = 53 * hash + Objects.hashCode(this.comments);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Redeem other = (Redeem) obj;
        if (this.type != other.type) {
            return false;
        }
        if (this.directEntrance != other.directEntrance) {
            return false;
        }
        if (!Objects.equals(this.comments, other.comments)) {
            return false;
        }
        return true;
    }

}
