package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "ActivityType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum ActivityType {

	TICKET("E"), // (E)ntrada
	EXCURSION("X"), // e(X)cursión
	COCHE("C"), // (C)ar hire
	BUNDLE("E"), // (B)undle
	UNKNOWN("?");
	
	private String code;
	
	private ActivityType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return this.code;
	}
	
	public static ActivityType fromActdis(String code) {
		for(ActivityType type: values()) {
			if(type.code.equals(code)) {
				return type;
			}
		}
		return UNKNOWN;
	}
	
}
