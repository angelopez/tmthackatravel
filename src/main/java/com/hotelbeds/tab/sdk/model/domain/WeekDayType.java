package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;
import java.util.Calendar;
import java.util.Date;

@XmlEnum
@XmlType(name = "WeekDayType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum WeekDayType
{

    SUNDAY(Calendar.SUNDAY),
    MONDAY(Calendar.MONDAY),
    TUESDAY(Calendar.TUESDAY),
    WEDNESDAY(Calendar.WEDNESDAY),
    THURSDAY(Calendar.THURSDAY),
    FRIDAY(Calendar.FRIDAY),
    SATURDAY(Calendar.SATURDAY);

    private int dayCode;

    private WeekDayType(int dayCode) {
        this.dayCode = dayCode;
    }

    public int getDayCode() {
        return dayCode;
    }

    public static WeekDayType fromDayCode(int dayCode) {
        for (WeekDayType dayOfWeek : WeekDayType.values()) {
            if (dayOfWeek.getDayCode() == dayCode) {
                return dayOfWeek;
            }
        }
        throw new IllegalArgumentException("Unkwown day of week code: " + dayCode);
    }

    public static WeekDayType fromDate(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("Null date is not allowed in DayOfWeek");
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        return fromDayCode(dayOfWeek);
    }
}
