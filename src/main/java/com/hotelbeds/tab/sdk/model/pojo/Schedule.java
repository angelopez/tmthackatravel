package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@XmlType(name = "Schedule",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "duration", "closed", "opened"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Schedule {

    
    private Duration           duration;
    
    private List<ScheduleInfo> closed;
    
    private List<ScheduleInfo> opened;

    public Schedule() {
    }

    public Schedule(List<ScheduleInfo> closed, List<ScheduleInfo> opened, Duration duration) {
        super();
        this.closed = closed;
        this.opened = opened;
        this.duration = duration;
    }

    public List<ScheduleInfo> getClosed() {
        return closed;
    }

    public void setClosed(List<ScheduleInfo> closed) {
        this.closed = closed;
    }

    public List<ScheduleInfo> getOpened() {
        return opened;
    }

    public void setOpened(List<ScheduleInfo> opened) {
        this.opened = opened;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Duration=").append(duration).append(",");
        sb.append("Closed=").append(closed).append(",");
        sb.append("Opened=").append(opened);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.duration);
        hash = 79 * hash + Objects.hashCode(this.closed);
        hash = 79 * hash + Objects.hashCode(this.opened);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Schedule other = (Schedule) obj;
        if (!Objects.equals(this.duration, other.duration)) {
            return false;
        }
        if (!Objects.equals(this.closed, other.closed)) {
            return false;
        }
        if (!Objects.equals(this.opened, other.opened)) {
            return false;
        }
        return true;
    }
    
    

}
