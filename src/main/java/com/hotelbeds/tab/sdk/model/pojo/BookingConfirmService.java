package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class BookingConfirmService {

    private String activityId; //serviceId
    private String rateKey; //purchaseableServiceId
    private List<Pax> paxes;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date from;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date to;
    private String zoneCode;
    private String session;
    private String language;
    private List<QuestionResponse> answers;
    private List<String> comments;
    private List<BookingExtraData> extraData;
    private String preferedLanguage;
    private String serviceLanguage;

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getRateKey() {
        return rateKey;
    }

    public void setRateKey(String rateKey) {
        this.rateKey = rateKey;
    }

    public List<Pax> getPaxes() {
        return paxes;
    }

    public void setPaxes(List<Pax> paxes) {
        this.paxes = paxes;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getZoneCode() {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<QuestionResponse> getAnswers() {
        return answers;
    }

    public void setAnswers(List<QuestionResponse> answers) {
        this.answers = answers;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public List<BookingExtraData> getExtraData() {
        return extraData;
    }

    public void setExtraData(List<BookingExtraData> extraData) {
        this.extraData = extraData;
    }

    public String getPreferedLanguage() {
        return preferedLanguage;
    }

    public void setPreferedLanguage(String preferedLanguage) {
        this.preferedLanguage = preferedLanguage;
    }

    public String getServiceLanguage() {
        return serviceLanguage;
    }

    public void setServiceLanguage(String serviceLanguage) {
        this.serviceLanguage = serviceLanguage;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.activityId);
        hash = 71 * hash + Objects.hashCode(this.rateKey);
        hash = 71 * hash + Objects.hashCode(this.paxes);
        hash = 71 * hash + Objects.hashCode(this.from);
        hash = 71 * hash + Objects.hashCode(this.to);
        hash = 71 * hash + Objects.hashCode(this.zoneCode);
        hash = 71 * hash + Objects.hashCode(this.session);
        hash = 71 * hash + Objects.hashCode(this.language);
        hash = 71 * hash + Objects.hashCode(this.answers);
        hash = 71 * hash + Objects.hashCode(this.comments);
        hash = 71 * hash + Objects.hashCode(this.extraData);
        hash = 71 * hash + Objects.hashCode(this.preferedLanguage);
        hash = 71 * hash + Objects.hashCode(this.serviceLanguage);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookingConfirmService other = (BookingConfirmService) obj;
        if (!Objects.equals(this.activityId, other.activityId)) {
            return false;
        }
        if (!Objects.equals(this.rateKey, other.rateKey)) {
            return false;
        }
        if (!Objects.equals(this.paxes, other.paxes)) {
            return false;
        }
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.zoneCode, other.zoneCode)) {
            return false;
        }
        if (!Objects.equals(this.session, other.session)) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        if (!Objects.equals(this.answers, other.answers)) {
            return false;
        }
        if (!Objects.equals(this.comments, other.comments)) {
            return false;
        }
        if (!Objects.equals(this.extraData, other.extraData)) {
            return false;
        }
        if (!Objects.equals(this.preferedLanguage, other.preferedLanguage)) {
            return false;
        }
        if (!Objects.equals(this.serviceLanguage, other.serviceLanguage)) {
            return false;
        }
        return true;
    }
    
    

}
