package com.hotelbeds.tab.sdk.model.pojo;

import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;

@XmlType(name = "Country",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "code", "name", "destinations"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Country {

    
    private String            code;
    
    private String            name;
    
    private List<DuoTypeName> destinations;

    public Country() {
        super();
    }

    public Country(String code, String name, List<DuoTypeName> destinations) {
        super();
        this.code = code;
        this.name = name;
        this.destinations = destinations;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DuoTypeName> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<DuoTypeName> destinations) {
        this.destinations = destinations;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CountryCode=").append(code).append(",");
        sb.append("CountryName=").append(name).append(",");
        sb.append(destinations.toString());

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.code);
        hash = 43 * hash + Objects.hashCode(this.name);
        hash = 43 * hash + Objects.hashCode(this.destinations);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Country other = (Country) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.destinations, other.destinations)) {
            return false;
        }
        return true;
    }

    
}
