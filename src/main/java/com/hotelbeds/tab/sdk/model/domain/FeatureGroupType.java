package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "FeatureGroupType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum FeatureGroupType
{
    MEAL("1"),
    DRINKS("2"),
    TRANSPORT("3"),
    ASSISTANCE("4"),
    MATERIAL("5"),
    TICKET("6"),
    ACCOMMODATION("7"),
    UNKNOWN("UNKNOWN");

    private String code;

    private FeatureGroupType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static FeatureGroupType getFeatureGroupType(String code) {
        for (FeatureGroupType type : values()) {
            if (type.code.equals(code)) {
                return type;
            }
        }
        return UNKNOWN;
    }
}
