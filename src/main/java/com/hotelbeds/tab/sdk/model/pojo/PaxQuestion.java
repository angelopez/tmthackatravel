package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaxQuestion", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages", propOrder = {"code", "text", "required"})
public class PaxQuestion {

    
    private String code;

    
    private String text;

    
    private boolean required;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.code);
        hash = 97 * hash + Objects.hashCode(this.text);
        hash = 97 * hash + (this.required ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PaxQuestion other = (PaxQuestion) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (this.required != other.required) {
            return false;
        }
        return true;
    }

}
