package com.hotelbeds.tab.sdk.model.request;

import com.hotelbeds.tab.sdk.model.pojo.ServiceRemoveDetail;
import java.util.Objects;

import javax.xml.bind.annotation.*;

@XmlType(name = "BookingCreditModificationRemoveRequest")
public class BookingCreditModificationRemoveRequest extends AbstractBookingModificationRequest {

    private ServiceRemoveDetail serviceToRemove;

    public BookingCreditModificationRemoveRequest() {
	}
    
    public BookingCreditModificationRemoveRequest(
			ServiceRemoveDetail serviceToRemove) {
		super();
		this.serviceToRemove = serviceToRemove;
	}

	public ServiceRemoveDetail getServiceToRemove() {
        return serviceToRemove;
    }

    public void setServiceToRemove(ServiceRemoveDetail serviceToRemove) {
        this.serviceToRemove = serviceToRemove;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.serviceToRemove);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final BookingCreditModificationRemoveRequest other = (BookingCreditModificationRemoveRequest) obj;
        if (!Objects.equals(this.serviceToRemove, other.serviceToRemove)) {
            return false;
        }
        return true;
    }
    
    
}
