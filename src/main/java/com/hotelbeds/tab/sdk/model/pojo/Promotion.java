package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import java.util.Date;
import java.util.Objects;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "Promotion",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "code", "order", "name", "description", "dateFrom", "dateTo", "imagePath"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Promotion {
    
    private String code;
    
    private Integer order;
    
    private String name;
    
    private String description;
    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dateFrom;
    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dateTo;
    
    private String imagePath;

    public Promotion() {
    }

    public Promotion(Integer order, String code, String imagePath, String description, String name, Date dateFrom, Date dateTo) {
        this.order = order;
        this.code = code;
        this.imagePath = imagePath;
        this.description = description;
        this.name = name;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public Promotion(Integer order, String code) {
        this.order = order;
        this.code = code;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public String toString() {
        return "Promotion{" + "order=" + order + ", code=" + code + ", imagePath=" + imagePath +
                ", description=" + description + ", name=" + name + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.code);
        hash = 97 * hash + Objects.hashCode(this.order);
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.description);
        hash = 97 * hash + Objects.hashCode(this.dateFrom);
        hash = 97 * hash + Objects.hashCode(this.dateTo);
        hash = 97 * hash + Objects.hashCode(this.imagePath);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Promotion other = (Promotion) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.order, other.order)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.dateFrom, other.dateFrom)) {
            return false;
        }
        if (!Objects.equals(this.dateTo, other.dateTo)) {
            return false;
        }
        if (!Objects.equals(this.imagePath, other.imagePath)) {
            return false;
        }
        return true;
    }
    
    
}
