package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.FeatureIncExcType;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlType(name = "FeatureDetail",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "featureType", "description"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class FeatureDetail {

    
    private FeatureIncExcType featureType;
    
    private String            description;

    public FeatureDetail() {
        super();
    }

    public FeatureDetail(FeatureIncExcType featureType, String description) {
        super();
        this.featureType = featureType;
        this.description = description;
    }

    public FeatureIncExcType getFeatureType() {
        return featureType;
    }

    public void setFeatureType(FeatureIncExcType featureType) {
        this.featureType = featureType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FeatureType=").append(featureType).append(",");
        sb.append("Description=").append(description);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.featureType);
        hash = 97 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeatureDetail other = (FeatureDetail) obj;
        if (this.featureType != other.featureType) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return true;
    }
    
    

}
