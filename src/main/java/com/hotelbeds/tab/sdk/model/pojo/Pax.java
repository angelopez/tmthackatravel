package com.hotelbeds.tab.sdk.model.pojo;


import com.hotelbeds.tab.sdk.model.domain.PaxType;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlType(name = "Pax")
public class Pax {

    private PaxType type;
    private String  surname;
    private Integer         age;
    private List<PaxAnswer> answers;
    private String          name;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public PaxType getType() {
        return type;
    }

    public void setType(PaxType type) {
        this.type = type;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<PaxAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<PaxAnswer> answers) {
        this.answers = answers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.type);
        hash = 23 * hash + Objects.hashCode(this.surname);
        hash = 23 * hash + Objects.hashCode(this.age);
        hash = 23 * hash + Objects.hashCode(this.answers);
        hash = 23 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pax other = (Pax) obj;
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.surname, other.surname)) {
            return false;
        }
        if (!Objects.equals(this.age, other.age)) {
            return false;
        }
        if (!Objects.equals(this.answers, other.answers)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    

}
