package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "HotelCodePickupRequest",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "hotel", "tti", "giata"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class HotelCodePickupRequest {

    
    private Long   hotel;

    
    private String tti;

    
    private String giata;

    public Long getHotel() {
        return hotel;
    }

    public void setHotel(Long hotel) {
        this.hotel = hotel;
    }

    public String getTti() {
        return tti;
    }

    public void setTti(String tti) {
        this.tti = tti;
    }

    public String getGiata() {
        return giata;
    }

    public void setGiata(String giata) {
        this.giata = giata;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.hotel);
        hash = 37 * hash + Objects.hashCode(this.tti);
        hash = 37 * hash + Objects.hashCode(this.giata);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HotelCodePickupRequest other = (HotelCodePickupRequest) obj;
        if (!Objects.equals(this.hotel, other.hotel)) {
            return false;
        }
        if (!Objects.equals(this.tti, other.tti)) {
            return false;
        }
        if (!Objects.equals(this.giata, other.giata)) {
            return false;
        }
        return true;
    }
    
    
}
