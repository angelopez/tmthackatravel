package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import java.util.Date;
import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "BookingHolder")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingHolder  {

    private String surname;
    private List<String> telephones;
    private String name;
    private String title;
    private String email;
    private String address;
    private String zipCode;
    private Boolean mailing = false;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date mailUpdDate;
    private String country;

    public BookingHolder() {
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<String> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<String> telephones) {
        this.telephones = telephones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Boolean getMailing() {
        return mailing;
    }

    public void setMailing(Boolean mailing) {
        this.mailing = mailing;
    }

    public Date getMailUpdDate() {
        return mailUpdDate;
    }

    public void setMailUpdDate(Date mailUpdDate) {
        this.mailUpdDate = mailUpdDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.surname);
        hash = 43 * hash + Objects.hashCode(this.telephones);
        hash = 43 * hash + Objects.hashCode(this.name);
        hash = 43 * hash + Objects.hashCode(this.title);
        hash = 43 * hash + Objects.hashCode(this.email);
        hash = 43 * hash + Objects.hashCode(this.address);
        hash = 43 * hash + Objects.hashCode(this.zipCode);
        hash = 43 * hash + Objects.hashCode(this.mailing);
        hash = 43 * hash + Objects.hashCode(this.mailUpdDate);
        hash = 43 * hash + Objects.hashCode(this.country);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookingHolder other = (BookingHolder) obj;
        if (!Objects.equals(this.surname, other.surname)) {
            return false;
        }
        if (!Objects.equals(this.telephones, other.telephones)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.zipCode, other.zipCode)) {
            return false;
        }
        if (!Objects.equals(this.mailing, other.mailing)) {
            return false;
        }
        if (!Objects.equals(this.mailUpdDate, other.mailUpdDate)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        return true;
    }   
    
}
