package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "PaymentInformationResponse", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages", propOrder = {"type", "invoiceCompany", "info", "status"})
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentInformationResponse {

    
    private String type;

    
    private InvoiceCompany invoiceCompany;

    
    private String info;

    
    private String status;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public InvoiceCompany getInvoiceCompany() {
        return invoiceCompany;
    }

    public void setInvoiceCompany(InvoiceCompany invoiceCompany) {
        this.invoiceCompany = invoiceCompany;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.type);
        hash = 19 * hash + Objects.hashCode(this.invoiceCompany);
        hash = 19 * hash + Objects.hashCode(this.info);
        hash = 19 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PaymentInformationResponse other = (PaymentInformationResponse) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.invoiceCompany, other.invoiceCompany)) {
            return false;
        }
        if (!Objects.equals(this.info, other.info)) {
            return false;
        }
        if (!Objects.equals(this.status, other.status)) {
            return false;
        }
        return true;
    }


}
