package com.hotelbeds.tab.sdk.model.request;

import com.hotelbeds.tab.sdk.model.pojo.ServiceModifyDetail;
import java.util.Objects;

import javax.xml.bind.annotation.*;

@XmlType(name = "BookingCreditModificationModifyRequest")
public class BookingCreditModificationModifyRequest extends AbstractBookingModificationRequest {

    private ServiceModifyDetail serviceToModify;

    public BookingCreditModificationModifyRequest() {
	}
    
    public BookingCreditModificationModifyRequest(
			ServiceModifyDetail serviceToModify) {
		super();
		this.serviceToModify = serviceToModify;
	}

	public ServiceModifyDetail getServiceToModify() {
        return serviceToModify;
    }

    public void setServiceToModify(ServiceModifyDetail serviceToModify) {
        this.serviceToModify = serviceToModify;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.serviceToModify);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final BookingCreditModificationModifyRequest other = (BookingCreditModificationModifyRequest) obj;
        if (!Objects.equals(this.serviceToModify, other.serviceToModify)) {
            return false;
        }
        return true;
    }
    
    
}
