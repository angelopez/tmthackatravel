package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Objects;

@XmlType(name = "AgencyData", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages", propOrder = {"code","name","branch","branchName","comments","sucursal"})
@XmlAccessorType(XmlAccessType.FIELD)
public class AgencyData implements Serializable {

    private static final long serialVersionUID = 1L;

    
    private Integer           code;

    
    private String            name;

    
    private Integer           branch;

    
    private String            branchName;

    
    private String            comments;

    
    private SucursalData      sucursal;

    public AgencyData() {
        super();
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getBranch() {
        return branch;
    }

    public void setBranch(Integer branch) {
        this.branch = branch;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public SucursalData getSucursal() {
        return sucursal;
    }

    public void setSucursal(SucursalData sucursal) {
        this.sucursal = sucursal;
    }

    @Override
    public String toString() {
        return "AgencyData [code=" + code + ", branch=" + branch + "]";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.code);
        hash = 73 * hash + Objects.hashCode(this.name);
        hash = 73 * hash + Objects.hashCode(this.branch);
        hash = 73 * hash + Objects.hashCode(this.branchName);
        hash = 73 * hash + Objects.hashCode(this.comments);
        hash = 73 * hash + Objects.hashCode(this.sucursal);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AgencyData other = (AgencyData) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.branch, other.branch)) {
            return false;
        }
        if (!Objects.equals(this.branchName, other.branchName)) {
            return false;
        }
        if (!Objects.equals(this.comments, other.comments)) {
            return false;
        }
        if (!Objects.equals(this.sucursal, other.sucursal)) {
            return false;
        }
        return true;
    }
    
    

}
