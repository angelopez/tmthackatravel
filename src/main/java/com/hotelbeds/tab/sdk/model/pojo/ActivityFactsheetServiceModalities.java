package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import java.util.Objects;

@XmlType(name = "ActivityFactsheetServiceModalities", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages", propOrder = {"activityCode","modalityCodes"})
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivityFactsheetServiceModalities {

    
    private String activityCode;

    
    private List<String> modalityCodes;

    public ActivityFactsheetServiceModalities() {
        super();
    }
    
    public ActivityFactsheetServiceModalities(String activityCode, List<String> modalityCodes) {
        super();
        this.activityCode = activityCode;
        this.modalityCodes = modalityCodes;
    }



    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }


    public List<String> getModalityCodes() {
        return modalityCodes;
    }

    public void setModalityCodes(List<String> modalityCodes) {
        this.modalityCodes = modalityCodes;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ActivityCode=" + activityCode).append(",");
        sb.append("ModalityCode=" + modalityCodes);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.activityCode);
        hash = 89 * hash + Objects.hashCode(this.modalityCodes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActivityFactsheetServiceModalities other = (ActivityFactsheetServiceModalities) obj;
        if (!Objects.equals(this.activityCode, other.activityCode)) {
            return false;
        }
        if (!Objects.equals(this.modalityCodes, other.modalityCodes)) {
            return false;
        }
        return true;
    }
    
    
    
}
