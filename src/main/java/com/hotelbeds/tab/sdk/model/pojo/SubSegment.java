package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlType(name = "SubSegment",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "code", "name", "order", "icon"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class SubSegment implements Comparable<SubSegment> {

    
    private Integer code;
    
    private String  name;
    
    private Integer order;
    
    private String  icon;

    public SubSegment() {
        super();
    }

    public SubSegment(Integer code, Integer order, String name, String icon) {
        super();
        this.code = code;
        this.order = order;
        this.name = name;
        this.icon = icon;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public int compareTo(SubSegment o) {
        return this.order.compareTo(o.order);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Code=").append(code).append(",");
        sb.append("Name=").append(name).append(",");
        sb.append("Order=").append(order).append(",");
        sb.append("Icon=").append(icon);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.code);
        hash = 71 * hash + Objects.hashCode(this.name);
        hash = 71 * hash + Objects.hashCode(this.order);
        hash = 71 * hash + Objects.hashCode(this.icon);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SubSegment other = (SubSegment) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.order, other.order)) {
            return false;
        }
        if (!Objects.equals(this.icon, other.icon)) {
            return false;
        }
        return true;
    }
    
    

}