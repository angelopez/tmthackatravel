package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "FeatureIncExcType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum FeatureIncExcType {
    DRINKSINCL,
    DRINKSNOT,
    EQUIPINCL,
    EQUIPNOT,
    FOODINCL,
    FOODNOT,
    ADMISSIONSINCL,
    ADMISSIONSNOT
}
