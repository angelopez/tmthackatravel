package com.hotelbeds.tab.sdk.model.request;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import com.hotelbeds.tab.sdk.model.pojo.PaxDistribution;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "ActivityDetailRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivityDetailRequest {

    
    private String code;

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date from;

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date to;

    
    private List<PaxDistribution> paxes;
    
    private String language;

    public ActivityDetailRequest() {
    	this.paxes = new ArrayList<PaxDistribution>();
	}
    
    public ActivityDetailRequest(String code, Date from, Date to,
			List<PaxDistribution> paxes, String language) {
		super();
		this.code = code;
		this.from = from;
		this.to = to;
		this.paxes = paxes;
		this.language = language;
	}

	public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public List<PaxDistribution> getPaxes() {
        return paxes;
    }

    public void setPaxes(List<PaxDistribution> paxes) {
        this.paxes = paxes;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    @Override
    public String toString() {
        //TODO: Falta por determiniar los campos que se muestra. Primera Propuesta
        StringBuilder sb = new StringBuilder();
        sb.append("ActivityCode=" + code).append(",");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (from != null) {
            sb.append("DateFrom=" + sdf.format(from)).append(",");
        }
        if (to != null) {
            sb.append("DateTo=" + sdf.format(to)).append(",");
        }

        sb.append("class=ActivityDetailRequest");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.code);
        hash = 47 * hash + Objects.hashCode(this.from);
        hash = 47 * hash + Objects.hashCode(this.to);
        hash = 47 * hash + Objects.hashCode(this.paxes);
        hash = 47 * hash + Objects.hashCode(this.language);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActivityDetailRequest other = (ActivityDetailRequest) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.paxes, other.paxes)) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        return true;
    }
    
    

}
