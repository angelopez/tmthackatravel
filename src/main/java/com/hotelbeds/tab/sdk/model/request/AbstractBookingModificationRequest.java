package  com.hotelbeds.tab.sdk.model.request;

import java.util.Objects;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "AbstractBookingModificationRequest")
public abstract class AbstractBookingModificationRequest  {

    private String clientReference;
    private String agencyComments;

    public AbstractBookingModificationRequest() {
    	
	}
    
    public AbstractBookingModificationRequest(String clientReference,
			String agencyComments) {
		super();
		this.clientReference = clientReference;
		this.agencyComments = agencyComments;
	}

	public String getClientReference() {
        return clientReference;
    }

    public void setClientReference(String clientReference) {
        this.clientReference = clientReference;
    }

    public String getAgencyComments() {
        return agencyComments;
    }

    public void setAgencyComments(String agencyComments) {
        this.agencyComments = agencyComments;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.clientReference);
        hash = 79 * hash + Objects.hashCode(this.agencyComments);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractBookingModificationRequest other = (AbstractBookingModificationRequest) obj;
        if (!Objects.equals(this.clientReference, other.clientReference)) {
            return false;
        }
        if (!Objects.equals(this.agencyComments, other.agencyComments)) {
            return false;
        }
        return true;
    }
    
    

}
