package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import com.hotelbeds.tab.sdk.model.domain.ActivityType;
import com.hotelbeds.tab.sdk.model.domain.BookingStatus;
import java.util.Date;
import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "PurchasedService")
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchasedService {

    private List<BundledService> bundles;
    private ActivityPickup pickup;
    private String id;
    private Integer rateKey;
    private AgencyCommission agencyCommission;
    private String currency;
    private ContactInfo contactInfo;
    private AmountDetail amountDetail;
    private List<BookingExtraData> extraData;
    private ProviderInformation providerInformation;
    private FactsheetActivity content;
    private String activityReference; // serviceReference
    private String code; // activityCode
    private String name; // activityName
    private Modality modality;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dateFrom;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dateTo;
    private PriceBreakdown rateBreakdown;
    private List<CancellationPolicy> cancellationPolicies;
    private List<PaxResponse> paxes;
    private List<QuestionResponse> questions;
    private BookingStatus status;
    private SupplierInformation supplier;
    private List<Comment> comments;
    private ActivityType type;
    private String priceType;
    private String externalSupplierReference;
    private List<Voucher> vouchers;

    public List<BundledService> getBundles() {
        return bundles;
    }

    public void setBundles(List<BundledService> bundles) {
        this.bundles = bundles;
    }

    public ActivityPickup getPickup() {
        return pickup;
    }

    public void setPickup(ActivityPickup pickup) {
        this.pickup = pickup;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getRateKey() {
        return rateKey;
    }

    public void setRateKey(Integer rateKey) {
        this.rateKey = rateKey;
    }

    public AgencyCommission getAgencyCommission() {
        return agencyCommission;
    }

    public void setAgencyCommission(AgencyCommission agencyCommission) {
        this.agencyCommission = agencyCommission;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }

    public AmountDetail getAmountDetail() {
        return amountDetail;
    }

    public void setAmountDetail(AmountDetail amountDetail) {
        this.amountDetail = amountDetail;
    }

    public List<BookingExtraData> getExtraData() {
        return extraData;
    }

    public void setExtraData(List<BookingExtraData> extraData) {
        this.extraData = extraData;
    }

    public ProviderInformation getProviderInformation() {
        return providerInformation;
    }

    public void setProviderInformation(ProviderInformation providerInformation) {
        this.providerInformation = providerInformation;
    }

    public FactsheetActivity getContent() {
        return content;
    }

    public void setContent(FactsheetActivity content) {
        this.content = content;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getActivityReference() {
        return activityReference;
    }

    public void setActivityReference(String activityReference) {
        this.activityReference = activityReference;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Modality getModality() {
        return modality;
    }

    public void setModality(Modality modality) {
        this.modality = modality;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public PriceBreakdown getRateBreakdown() {
        return rateBreakdown;
    }

    public void setRateBreakdown(PriceBreakdown rateBreakdown) {
        this.rateBreakdown = rateBreakdown;
    }

    public List<CancellationPolicy> getCancellationPolicies() {
        return cancellationPolicies;
    }

    public void setCancellationPolicies(List<CancellationPolicy> cancellationPolicies) {
        this.cancellationPolicies = cancellationPolicies;
    }

    public List<PaxResponse> getPaxes() {
        return paxes;
    }

    public void setPaxes(List<PaxResponse> paxes) {
        this.paxes = paxes;
    }

    public List<QuestionResponse> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionResponse> questions) {
        this.questions = questions;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    public SupplierInformation getSupplier() {
        return supplier;
    }

    public void setSupplier(SupplierInformation supplier) {
        this.supplier = supplier;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public ActivityType getType() {
        return type;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }

    public String getExternalSupplierReference() {
        return externalSupplierReference;
    }

    public void setExternalSupplierReference(String externalSupplierReference) {
        this.externalSupplierReference = externalSupplierReference;
    }

    public List<Voucher> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<Voucher> vouchers) {
        this.vouchers = vouchers;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.bundles);
        hash = 53 * hash + Objects.hashCode(this.pickup);
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.rateKey);
        hash = 53 * hash + Objects.hashCode(this.agencyCommission);
        hash = 53 * hash + Objects.hashCode(this.currency);
        hash = 53 * hash + Objects.hashCode(this.contactInfo);
        hash = 53 * hash + Objects.hashCode(this.amountDetail);
        hash = 53 * hash + Objects.hashCode(this.extraData);
        hash = 53 * hash + Objects.hashCode(this.providerInformation);
        hash = 53 * hash + Objects.hashCode(this.content);
        hash = 53 * hash + Objects.hashCode(this.activityReference);
        hash = 53 * hash + Objects.hashCode(this.code);
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + Objects.hashCode(this.modality);
        hash = 53 * hash + Objects.hashCode(this.dateFrom);
        hash = 53 * hash + Objects.hashCode(this.dateTo);
        hash = 53 * hash + Objects.hashCode(this.rateBreakdown);
        hash = 53 * hash + Objects.hashCode(this.cancellationPolicies);
        hash = 53 * hash + Objects.hashCode(this.paxes);
        hash = 53 * hash + Objects.hashCode(this.questions);
        hash = 53 * hash + Objects.hashCode(this.status);
        hash = 53 * hash + Objects.hashCode(this.supplier);
        hash = 53 * hash + Objects.hashCode(this.comments);
        hash = 53 * hash + Objects.hashCode(this.type);
        hash = 53 * hash + Objects.hashCode(this.priceType);
        hash = 53 * hash + Objects.hashCode(this.externalSupplierReference);
        hash = 53 * hash + Objects.hashCode(this.vouchers);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PurchasedService other = (PurchasedService) obj;
        if (!Objects.equals(this.bundles, other.bundles)) {
            return false;
        }
        if (!Objects.equals(this.pickup, other.pickup)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.rateKey, other.rateKey)) {
            return false;
        }
        if (!Objects.equals(this.agencyCommission, other.agencyCommission)) {
            return false;
        }
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        if (!Objects.equals(this.contactInfo, other.contactInfo)) {
            return false;
        }
        if (!Objects.equals(this.amountDetail, other.amountDetail)) {
            return false;
        }
        if (!Objects.equals(this.extraData, other.extraData)) {
            return false;
        }
        if (!Objects.equals(this.providerInformation, other.providerInformation)) {
            return false;
        }
        if (!Objects.equals(this.content, other.content)) {
            return false;
        }
        if (!Objects.equals(this.activityReference, other.activityReference)) {
            return false;
        }
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.modality, other.modality)) {
            return false;
        }
        if (!Objects.equals(this.dateFrom, other.dateFrom)) {
            return false;
        }
        if (!Objects.equals(this.dateTo, other.dateTo)) {
            return false;
        }
        if (!Objects.equals(this.rateBreakdown, other.rateBreakdown)) {
            return false;
        }
        if (!Objects.equals(this.cancellationPolicies, other.cancellationPolicies)) {
            return false;
        }
        if (!Objects.equals(this.paxes, other.paxes)) {
            return false;
        }
        if (!Objects.equals(this.questions, other.questions)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.supplier, other.supplier)) {
            return false;
        }
        if (!Objects.equals(this.comments, other.comments)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.priceType, other.priceType)) {
            return false;
        }
        if (!Objects.equals(this.externalSupplierReference, other.externalSupplierReference)) {
            return false;
        }
        if (!Objects.equals(this.vouchers, other.vouchers)) {
            return false;
        }
        return true;
    }
    
    

}
