package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.GuideGroupType;
import com.hotelbeds.tab.sdk.model.domain.GuideTipType;
import com.hotelbeds.tab.sdk.model.domain.GuideType;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "GuidingOptions",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "groupType", "guideType", "included", "maxGroupSize", "tips"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class GuidingOptions {

    
    private GuideGroupType groupType;
    
    private GuideType      guideType;
    
    private boolean        included;
    
    private Integer        maxGroupSize;
    
    private GuideTipType   tips;

    public GuidingOptions() {
        super();
    }

    public GuidingOptions(GuideGroupType groupType, GuideType guideType, boolean included, Integer maxGroupSize, GuideTipType tips) {
        super();
        this.groupType = groupType;
        this.guideType = guideType;
        this.included = included;
        this.maxGroupSize = maxGroupSize;
        this.tips = tips;
    }

    public GuideGroupType getGroupType() {
        return groupType;
    }

    public void setGroupType(GuideGroupType groupType) {
        this.groupType = groupType;
    }

    public GuideType getGuideType() {
        return guideType;
    }

    public void setGuideType(GuideType guideType) {
        this.guideType = guideType;
    }

    public boolean isIncluded() {
        return included;
    }

    public void setIncluded(boolean included) {
        this.included = included;
    }

    public Integer getMaxGroupSize() {
        return maxGroupSize;
    }

    public void setMaxGroupSize(Integer maxGroupSize) {
        this.maxGroupSize = maxGroupSize;
    }

    public GuideTipType getTips() {
        return tips;
    }

    public void setTips(GuideTipType tips) {
        this.tips = tips;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GroupType=").append(groupType).append(",");
        sb.append("GuideType=").append(guideType).append(",");
        sb.append("Included=").append(included).append(",");
        sb.append("MaxGroupSize=").append(maxGroupSize).append(",");
        sb.append("Tips=").append(tips).append(",");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.groupType);
        hash = 37 * hash + Objects.hashCode(this.guideType);
        hash = 37 * hash + (this.included ? 1 : 0);
        hash = 37 * hash + Objects.hashCode(this.maxGroupSize);
        hash = 37 * hash + Objects.hashCode(this.tips);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GuidingOptions other = (GuidingOptions) obj;
        if (this.groupType != other.groupType) {
            return false;
        }
        if (this.guideType != other.guideType) {
            return false;
        }
        if (this.included != other.included) {
            return false;
        }
        if (!Objects.equals(this.maxGroupSize, other.maxGroupSize)) {
            return false;
        }
        if (this.tips != other.tips) {
            return false;
        }
        return true;
    }
    
    

}
