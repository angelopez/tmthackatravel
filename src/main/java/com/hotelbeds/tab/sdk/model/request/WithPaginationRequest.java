package  com.hotelbeds.tab.sdk.model.request;


import com.hotelbeds.tab.sdk.model.pojo.PaginationRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.NONE)
public interface WithPaginationRequest {

    PaginationRequest getPagination();

}
