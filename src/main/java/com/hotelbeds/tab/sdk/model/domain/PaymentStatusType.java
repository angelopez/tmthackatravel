package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by xbibiloni on 29/02/2016.
 */
@XmlEnum
@XmlType(name = "PaymentStatusType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum PaymentStatusType {

    PARCIAL_PAY("P", "PARCIAL_PAY"),
    TOTAL_PAY("T", "TOTAL_PAY"),
    NOT_PAY("N", "NOT_PAY"),
    ALL("","ALL");

    private String filterCode;
    private String paymentStatusCode;

    private PaymentStatusType (String filterCode, String paymentStatusCode) {
        this.filterCode = filterCode;
        this.paymentStatusCode = paymentStatusCode;
    }

    public String getFilterCode () {
        return this.filterCode;
    }
    public String getPaymenStatucCode () {
        return this.paymentStatusCode;
    }

    public static PaymentStatusType getPaymentStatusType (String paymentStatusCode) {
        for (PaymentStatusType paymentStatusType: PaymentStatusType.values()) {
            if (paymentStatusType.getPaymenStatucCode().equals(paymentStatusCode)) {
                return paymentStatusType;
            }
        }
        return null;
    }

}
