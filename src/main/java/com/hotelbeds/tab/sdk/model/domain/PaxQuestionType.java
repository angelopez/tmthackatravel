package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "PaxQuestionType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum PaxQuestionType {
	NAME("Name", true),
	LASTNAME("Last name", true),
	PASSPORT("Passport", true);
	
	private String text;
	private boolean required;
	
	private PaxQuestionType(String text, boolean required) {
		this.text = text;
		this.required = required;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
	
}
