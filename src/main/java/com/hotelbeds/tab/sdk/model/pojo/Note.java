package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "Note",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "dateFrom", "dateTo", "visibleFrom", "visibleTo", "descriptions"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Note {

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date                     dateFrom;
    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date                     dateTo;
    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date                     visibleFrom;
    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date                     visibleTo;
    
    private List<DuoTypeDescription> descriptions;

    public Note() {
        super();
    }

    public Note(Date dateFrom, Date dateTo, Date visibleFrom, Date visibleTo, List<DuoTypeDescription> descriptions) {
        super();
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.visibleFrom = visibleFrom;
        this.visibleTo = visibleTo;
        this.descriptions = descriptions;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date from) {
        this.dateFrom = from;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date to) {
        this.dateTo = to;
    }

    public Date getVisibleFrom() {
        return visibleFrom;
    }

    public void setVisibleFrom(Date visibleFrom) {
        this.visibleFrom = visibleFrom;
    }

    public Date getVisibleTo() {
        return visibleTo;
    }

    public void setVisibleTo(Date visibleTo) {
        this.visibleTo = visibleTo;
    }

    public List<DuoTypeDescription> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DuoTypeDescription> descriptions) {
        this.descriptions = descriptions;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DateFrom=").append(dateFrom).append(",");
        sb.append("DateTo=").append(dateTo).append(",");
        sb.append("VisibleFrom=").append(visibleFrom).append(",");
        sb.append("VisibleTo=").append(visibleTo).append(",");
        sb.append("Descriptions=").append(descriptions);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.dateFrom);
        hash = 97 * hash + Objects.hashCode(this.dateTo);
        hash = 97 * hash + Objects.hashCode(this.visibleFrom);
        hash = 97 * hash + Objects.hashCode(this.visibleTo);
        hash = 97 * hash + Objects.hashCode(this.descriptions);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Note other = (Note) obj;
        if (!Objects.equals(this.dateFrom, other.dateFrom)) {
            return false;
        }
        if (!Objects.equals(this.dateTo, other.dateTo)) {
            return false;
        }
        if (!Objects.equals(this.visibleFrom, other.visibleFrom)) {
            return false;
        }
        if (!Objects.equals(this.visibleTo, other.visibleTo)) {
            return false;
        }
        if (!Objects.equals(this.descriptions, other.descriptions)) {
            return false;
        }
        return true;
    }
    
    

}
