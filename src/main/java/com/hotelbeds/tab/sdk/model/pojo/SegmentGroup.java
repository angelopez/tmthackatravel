package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@XmlType(name = "SegmentGroup",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "code", "name", "segments"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class SegmentGroup {

    
    private Long          code;
    
    private String        name;
    
    private List<Segment> segments;

    public SegmentGroup() {
    }

    public SegmentGroup(Long code, String name, List<Segment> segments) {
        super();
        this.code = code;
        this.name = name;
        this.segments = segments;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Code=").append(code).append(",");
        sb.append("Name=").append(name).append(",");
        sb.append("Segments=").append(segments);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.code);
        hash = 23 * hash + Objects.hashCode(this.name);
        hash = 23 * hash + Objects.hashCode(this.segments);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SegmentGroup other = (SegmentGroup) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.segments, other.segments)) {
            return false;
        }
        return true;
    }
    
    

}
