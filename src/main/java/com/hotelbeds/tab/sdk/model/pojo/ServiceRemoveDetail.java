package com.hotelbeds.tab.sdk.model.pojo;


import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@XmlType(name = "ServiceRemoveDetail", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceRemoveDetail implements Serializable {

    private static final long      serialVersionUID = 1L;

     
    private String                 activityId;

     
    private List<BookingExtraData> serviceExtraData;

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public List<BookingExtraData> getServiceExtraData() {
        return serviceExtraData;
    }

    public void setServiceExtraData(List<BookingExtraData> serviceExtraData) {
        this.serviceExtraData = serviceExtraData;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.activityId);
        hash = 53 * hash + Objects.hashCode(this.serviceExtraData);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ServiceRemoveDetail other = (ServiceRemoveDetail) obj;
        if (!Objects.equals(this.activityId, other.activityId)) {
            return false;
        }
        if (!Objects.equals(this.serviceExtraData, other.serviceExtraData)) {
            return false;
        }
        return true;
    }


}
