package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import java.util.Objects;

@XmlType(name = "Media",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "audios", "documents", "images", "others", "videos"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Media {

    
    private List<MediaOther> audios;
    
    private List<MediaOther> documents;
    
    private List<MediaOther> images;
    
    private List<MediaOther> others;
    
    private List<MediaVideo> videos;

    public Media() {
    }

    public Media(List<MediaOther> audios, List<MediaOther> documents, List<MediaOther> images, List<MediaOther> others, List<MediaVideo> videos) {
        super();
        this.audios = audios;
        this.documents = documents;
        this.images = images;
        this.others = others;
        this.videos = videos;
    }

    public List<MediaOther> getAudios() {
        return audios;
    }

    public void setAudios(List<MediaOther> audios) {
        this.audios = audios;
    }

    public List<MediaOther> getDocuments() {
        return documents;
    }

    public void setDocuments(List<MediaOther> documents) {
        this.documents = documents;
    }

    public List<MediaOther> getImages() {
        return images;
    }

    public void setImages(List<MediaOther> images) {
        this.images = images;
    }

    public List<MediaOther> getOthers() {
        return others;
    }

    public void setOthers(List<MediaOther> others) {
        this.others = others;
    }

    public List<MediaVideo> getVideos() {
        return videos;
    }

    public void setVideos(List<MediaVideo> videos) {
        this.videos = videos;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Audios=").append(audios).append(",");
        sb.append("Documents=").append(documents).append(",");
        sb.append("Images=").append(images).append(",");
        sb.append("Others=").append(others).append(",");
        sb.append("Videos=").append(videos).append(",");

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.audios);
        hash = 79 * hash + Objects.hashCode(this.documents);
        hash = 79 * hash + Objects.hashCode(this.images);
        hash = 79 * hash + Objects.hashCode(this.others);
        hash = 79 * hash + Objects.hashCode(this.videos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Media other = (Media) obj;
        if (!Objects.equals(this.audios, other.audios)) {
            return false;
        }
        if (!Objects.equals(this.documents, other.documents)) {
            return false;
        }
        if (!Objects.equals(this.images, other.images)) {
            return false;
        }
        if (!Objects.equals(this.others, other.others)) {
            return false;
        }
        if (!Objects.equals(this.videos, other.videos)) {
            return false;
        }
        return true;
    }
    
    
}
