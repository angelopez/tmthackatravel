package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "PaginationRequest",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = {"itemsPerPage", "page"})
@XmlAccessorType(XmlAccessType.FIELD)
public class PaginationRequest {

    
    private Integer itemsPerPage;
    
    private Integer page;

    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "PaginationRequest{"
                + "itemsPerPage=" + itemsPerPage
                + ", page=" + page
                + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.itemsPerPage);
        hash = 71 * hash + Objects.hashCode(this.page);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PaginationRequest other = (PaginationRequest) obj;
        if (!Objects.equals(this.itemsPerPage, other.itemsPerPage)) {
            return false;
        }
        if (!Objects.equals(this.page, other.page)) {
            return false;
        }
        return true;
    }
    
    
    
}
