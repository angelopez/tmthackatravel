package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@XmlType(name = "OperationDate",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "from", "to", "cancellationPolicies" } )
@XmlAccessorType(XmlAccessType.FIELD)
public class OperationDate {

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date from;

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date to;

    
    private List<CancellationPolicy> cancellationPolicies;

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public List<CancellationPolicy> getCancellationPolicies() {
        return cancellationPolicies;
    }

    public void setCancellationPolicies(List<CancellationPolicy> cancellationPolicies) {
        this.cancellationPolicies = cancellationPolicies;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.from);
        hash = 71 * hash + Objects.hashCode(this.to);
        hash = 71 * hash + Objects.hashCode(this.cancellationPolicies);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OperationDate other = (OperationDate) obj;
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.cancellationPolicies, other.cancellationPolicies)) {
            return false;
        }
        return true;
    }
    
    

}
