package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlType(name = "Venue",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "type", "value"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Venue {

    
    private String type;
    
    private String value;

    public Venue() {
    }

    public Venue(String type, String value) {
        super();
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Type=").append(type).append(",");
        sb.append("Value=").append(value);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.type);
        hash = 71 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Venue other = (Venue) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
    
    

}
