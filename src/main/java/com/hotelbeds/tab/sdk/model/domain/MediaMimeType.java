package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "MediaMimeType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum MediaMimeType
{
    MP3("audio/mpeg3"),
    PDF("application/pdf"),
    PNG("image/png"),
    MPEG4("video/mpeg4"),
    AVI("video/avi"),
    JPG("image/jpeg"),
    GIF("image/gif"),
    TEXT_PLAIN("text/plain"),
    HTML("text/html"),
    WORD ("application/msword");

    private String code;

    MediaMimeType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static MediaMimeType fromCode(String code) {
        for (MediaMimeType type : MediaMimeType.values()) {
            if (type.getCode().equalsIgnoreCase(code)) {
                return type;
            }
        }
        return PDF;
    }

    @Override
    public String toString() {
        return this.code;
    }

}
