package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;
import java.util.Arrays;
import java.util.List;

@XmlEnum
@XmlType(name = "ActivityFilterType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum ActivityFilterType
{

    HOTEL_ID("hotel"),
    HOTEL_GIATA("giata"),
    HOTEL_TTI("tti"),
    DESTINATION_ID("destination"),
    DESTINATION_ISO("destination-iso"),
    COORDINATES_GPS("gps"),
    FACTSHEET_ID("factsheet"),
    TEXT("text"),
    SEGMENT("segment"),
    COUNTRY_ID("country"),
    SERVICE("service"),
    SERVICE_MODALITY("service_modality"),
    PRICEFROM("priceFrom"),
    PRICETO("priceTo");

    private String type;

    ActivityFilterType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public static ActivityFilterType fromType(String type) {
        for (ActivityFilterType dType : values()) {
            if (dType.type.equalsIgnoreCase(type)) {
                return dType;
            }
        }
        return null;
    }

    public static String getAllowedTypes() {
        StringBuilder sb = new StringBuilder();
        for(ActivityFilterType filter: values()) {
            if(sb.length() > 0) { sb.append(", "); }
            sb.append(filter.getType());
        }
        return sb.toString();
    }
    
    public static List<ActivityFilterType> getHotelFilters() {
        return Arrays.asList(HOTEL_ID, HOTEL_GIATA, HOTEL_TTI);
    }
    
    public static List<ActivityFilterType> getDestinationFilters() {
        return Arrays.asList(DESTINATION_ID, DESTINATION_ISO);
    }
    
    public static List<ActivityFilterType> getPriceFilters() {
        return Arrays.asList(PRICEFROM, PRICETO);
    }
}
