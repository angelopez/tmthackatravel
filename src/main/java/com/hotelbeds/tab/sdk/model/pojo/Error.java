package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlType(name = "Error", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages", propOrder = {"code", "text", "internalDescription"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Error {

    
    private String code;

    
    private String text;

    
    private String internalDescription;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getInternalDescription() {
        return internalDescription;
    }

    public void setInternalDescription(String internalDescription) {
        this.internalDescription = internalDescription;
    }

    @Override
    public String toString() {
        return "Error [code=" + code + ", clientDescription="
                + text + ", internalDescription="
                + internalDescription + "]";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.code);
        hash = 71 * hash + Objects.hashCode(this.text);
        hash = 71 * hash + Objects.hashCode(this.internalDescription);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Error other = (Error) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.internalDescription, other.internalDescription)) {
            return false;
        }
        return true;
    }
    
    

}
