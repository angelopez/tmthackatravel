package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.BookingExtraDataType;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingExtraData", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages", propOrder = { "id", "value"} )
public class BookingExtraData {

    
    private BookingExtraDataType id;

    
    private String               value;

    public BookingExtraData() {
    }

    public BookingExtraData(BookingExtraDataType id, String value) {
        super();
        this.id = id;
        this.value = value;
    }

    public BookingExtraDataType getId() {
        return id;
    }

    public void setId(BookingExtraDataType id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        hash = 89 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookingExtraData other = (BookingExtraData) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
    
    

}
