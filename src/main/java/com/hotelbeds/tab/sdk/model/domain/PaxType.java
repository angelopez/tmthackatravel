package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "PaxType", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlEnum
public enum PaxType
{

    ADULT("AD"),
    CHILD("CH"),
    INFANT("IN");

    private String code;

    PaxType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static PaxType fromCode(String code) {
        for (PaxType paxType : PaxType.values()) {
            if (paxType.getCode().equals(code)) {
                return paxType;
            }
        }
        return null;
    }
}
