package com.hotelbeds.tab.sdk.model.response;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "PaginationResponse",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = {"page", "itemsPerPage", "totalItems"})
@XmlAccessorType(XmlAccessType.FIELD)
public class PaginationResponse {

    
    private Integer itemsPerPage;

    
    private Integer page;

    
    private Integer totalItems;

    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + Objects.hashCode(this.itemsPerPage);
        hash = 13 * hash + Objects.hashCode(this.page);
        hash = 13 * hash + Objects.hashCode(this.totalItems);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final PaginationResponse other = (PaginationResponse) obj;
        if (!Objects.equals(this.itemsPerPage, other.itemsPerPage)) {
            return false;
        }
        if (!Objects.equals(this.page, other.page)) {
            return false;
        }
        if (!Objects.equals(this.totalItems, other.totalItems)) {
            return false;
        }
        return true;
    }

}
