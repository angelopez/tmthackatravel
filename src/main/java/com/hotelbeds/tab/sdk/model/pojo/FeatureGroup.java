package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.FeatureGroupType;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;

@XmlType(name = "FeatureGroup",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "groupCode", "excluded", "included"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class FeatureGroup {

    
    private FeatureGroupType    groupCode;
    
    private List<FeatureDetail> excluded;
    
    private List<FeatureDetail> included;

    public FeatureGroup() {
        super();
    }

    public FeatureGroup(FeatureGroupType groupCode, List<FeatureDetail> excluded, List<FeatureDetail> included) {
        super();
        this.groupCode = groupCode;
        this.excluded = excluded;
        this.included = included;
    }

    public FeatureGroupType getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(FeatureGroupType groupCode) {
        this.groupCode = groupCode;
    }

    public List<FeatureDetail> getExcluded() {
        return excluded;
    }

    public void setExcluded(List<FeatureDetail> excluded) {
        this.excluded = excluded;
    }

    public List<FeatureDetail> getIncluded() {
        return included;
    }

    public void setIncluded(List<FeatureDetail> included) {
        this.included = included;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GroupCode=").append(groupCode).append(",");
        sb.append("Excluded=").append(excluded).append(",");
        sb.append("Included=").append(included);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.groupCode);
        hash = 11 * hash + Objects.hashCode(this.excluded);
        hash = 11 * hash + Objects.hashCode(this.included);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeatureGroup other = (FeatureGroup) obj;
        if (this.groupCode != other.groupCode) {
            return false;
        }
        if (!Objects.equals(this.excluded, other.excluded)) {
            return false;
        }
        if (!Objects.equals(this.included, other.included)) {
            return false;
        }
        return true;
    }
    
    

}