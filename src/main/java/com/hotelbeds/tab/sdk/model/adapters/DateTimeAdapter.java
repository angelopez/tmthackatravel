package com.hotelbeds.tab.sdk.model.adapters;


import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.springframework.util.StringUtils;

public class DateTimeAdapter extends XmlAdapter<String, Date> {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    @Override
    public Date unmarshal(String v) throws Exception {
        if (StringUtils.isEmpty(v)) {
            return null;
        } else {
            return sdf.parse(v);
        }
    }

    @Override
    public String marshal(Date v) throws Exception {
        return v != null ? sdf.format(v) : null;
    }

    public static void main(String args[]) throws Exception {
        Date d = new Date();
        DateTimeAdapter adapter = new DateTimeAdapter();
        System.out.println(d);
        System.out.println(adapter.marshal(d));
        System.out.println(adapter.unmarshal(adapter.marshal(d)));
    }

}
