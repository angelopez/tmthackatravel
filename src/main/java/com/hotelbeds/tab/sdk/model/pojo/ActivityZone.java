package com.hotelbeds.tab.sdk.model.pojo;

import java.util.Objects;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ActivityZone")
public class ActivityZone  {

    private String zoneDescription;
    private Integer officeCode;
    private String  zoneCode;


    public ActivityZone() {
    }

    public String getZoneDescription() {
        return zoneDescription;
    }

    public void setZoneDescription(String zoneDescription) {
        this.zoneDescription = zoneDescription;
    }

    public Integer getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(Integer officeCode) {
        this.officeCode = officeCode;
    }

    public String getZoneCode() {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.zoneDescription);
        hash = 23 * hash + Objects.hashCode(this.officeCode);
        hash = 23 * hash + Objects.hashCode(this.zoneCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActivityZone other = (ActivityZone) obj;
        if (!Objects.equals(this.zoneDescription, other.zoneDescription)) {
            return false;
        }
        if (!Objects.equals(this.officeCode, other.officeCode)) {
            return false;
        }
        if (!Objects.equals(this.zoneCode, other.zoneCode)) {
            return false;
        }
        return true;
    }
    
    
    
}
