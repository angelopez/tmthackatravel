package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "ImageSize", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum ImageSize {

	SMALL("S","SMALL"),
	MEDIUM("M","MEDIUM"),
	LARGE("L","LARGE","BIG"),
	LARGE2("L2","LARGE2"),
	XLARGE("XL","XLARGE"),
	RAW("RAW", "RAW"),
	UNKNOWN("?");
	
	private String[] tabconType;
	
	private ImageSize(String... tabconType) {
		this.tabconType = tabconType;
	}
	
	public String getTabconType() {
		return this.tabconType[0];
	}
	
	public static ImageSize fromTABCON(String tabconType) {
		for(ImageSize size: ImageSize.values()) {
			for(String type :size.tabconType ){
				if(type.equalsIgnoreCase(tabconType)) {
					return size;
				}
			}

		}
		return UNKNOWN;
	}
	
}
