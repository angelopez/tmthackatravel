package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.DuoTypeName;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "LanguagesResponse")
@XmlType(name = "LanguagesResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.LanguagesResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class LanguagesResponse extends BaseResponse {

    
    private List<DuoTypeName> languages;

    public List<DuoTypeName> getLanguages() {
        return languages;
    }

    public void setLanguages(List<DuoTypeName> languages) {
        this.languages = languages;
    }

    @Override
    public String toString() {
        //TODO: Falta por determinar los atributos que se muestran. Primera propuesta
        StringBuilder sb = new StringBuilder();
        if (languages != null) {
            sb.append("NumOfLanguages=" + languages.size()).append(",");
        } else {
            sb.append("NumOfLanguages=0").append(",");
        }
        sb.append("class=LanguagesResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.languages);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final LanguagesResponse other = (LanguagesResponse) obj;
        if (!Objects.equals(this.languages, other.languages)) {
            return false;
        }
        return true;
    }

}
