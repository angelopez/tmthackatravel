package com.hotelbeds.tab.sdk.model.pojo;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import java.util.Objects;

@XmlType(name = "ActivitySearchFilterItemList", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivitySearchFilterItemList {

     
    private List<ActivitySearchFilterItem> searchFilterItems;

    public List<ActivitySearchFilterItem> getSearchFilterItems() {
        return searchFilterItems;
    }

    public void setSearchFilterItems(List<ActivitySearchFilterItem> searchFilterItems) {
        this.searchFilterItems = searchFilterItems;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.searchFilterItems);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActivitySearchFilterItemList other = (ActivitySearchFilterItemList) obj;
        if (!Objects.equals(this.searchFilterItems, other.searchFilterItems)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
