package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.Booking;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "BookingListResponse")
@XmlType(name = "BookingListResponse", namespace = "com.hotelbeds.tab.actapi.v3.response.BookingListResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingListResponse extends BaseResponse {

    
    private List<Booking> bookings;

    
    private PaginationResponse pagination;

    public PaginationResponse getPagination() {
        return pagination;
    }

    public void setPagination(PaginationResponse pagination) {
        this.pagination = pagination;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    @Override
    public String toString() {
        //TODO: Falta por determinar los atributos que se muestran. Primera propuesta
        StringBuilder sb = new StringBuilder(super.toString()); // auditData
        if (bookings != null) {
            sb.append("NumOfBookings=" + bookings.size()).append(",");
        } else {
            sb.append("NumOfBookings=0").append(",");
        }
        sb.append("class=BookingListResponse");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.bookings);
        hash = 47 * hash + Objects.hashCode(this.pagination);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final BookingListResponse other = (BookingListResponse) obj;
        if (!Objects.equals(this.bookings, other.bookings)) {
            return false;
        }
        if (!Objects.equals(this.pagination, other.pagination)) {
            return false;
        }
        return true;
    }
    
    
    
}
