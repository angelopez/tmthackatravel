package com.hotelbeds.tab.sdk.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "ActivityFactsheetType",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public enum ActivityFactsheetType
{
    TOURS,
    ATTRACCTIONS,
    ACTIVITIES,
    EVENTS,
    HOP_ON_HOP_OFF,
    BUNDLE,
    PASS_CARDS
}
