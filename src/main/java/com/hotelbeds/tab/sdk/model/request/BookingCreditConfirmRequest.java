package com.hotelbeds.tab.sdk.model.request;

import com.hotelbeds.tab.sdk.model.pojo.BookingConfirmService;
import com.hotelbeds.tab.sdk.model.pojo.BookingHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BookingCreditConfirmRequest")
public class BookingCreditConfirmRequest {

    private String reference;
    private String language;
    private String clientReference;
    private BookingHolder holder;
    private List<BookingConfirmService> activities;

    public BookingCreditConfirmRequest() {
        this.activities = new ArrayList<BookingConfirmService>();
    }

    public BookingCreditConfirmRequest(String reference, String language,
            String clientReference, BookingHolder holder,
            List<BookingConfirmService> activities) {
        super();
        this.reference = reference;
        this.language = language;
        this.clientReference = clientReference;
        this.holder = holder;
        this.activities = activities;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getClientReference() {
        return clientReference;
    }

    public void setClientReference(String clientReference) {
        this.clientReference = clientReference;
    }

    public BookingHolder getHolder() {
        return holder;
    }

    public void setHolder(BookingHolder holder) {
        this.holder = holder;
    }

    public List<BookingConfirmService> getActivities() {
        return activities;
    }

    public void setActivities(List<BookingConfirmService> activities) {
        this.activities = activities;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.reference);
        hash = 29 * hash + Objects.hashCode(this.language);
        hash = 29 * hash + Objects.hashCode(this.clientReference);
        hash = 29 * hash + Objects.hashCode(this.holder);
        hash = 29 * hash + Objects.hashCode(this.activities);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookingCreditConfirmRequest other = (BookingCreditConfirmRequest) obj;
        if (!Objects.equals(this.reference, other.reference)) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        if (!Objects.equals(this.clientReference, other.clientReference)) {
            return false;
        }
        if (!Objects.equals(this.holder, other.holder)) {
            return false;
        }
        if (!Objects.equals(this.activities, other.activities)) {
            return false;
        }
        return true;
    }

}
