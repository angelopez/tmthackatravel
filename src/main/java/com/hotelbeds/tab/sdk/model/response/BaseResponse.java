package com.hotelbeds.tab.sdk.model.response;

import com.hotelbeds.tab.sdk.model.pojo.AuditData;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.*;
import com.hotelbeds.tab.sdk.model.pojo.Error;

@XmlType(name = "BaseResponse")
public abstract class BaseResponse {

    private AuditData auditData;
    private String operationId;
    private List<Error> errors;

    public AuditData getAuditData() {
        return auditData;
    }

    public void setAuditData(AuditData auditData) {
        this.auditData = auditData;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.auditData);
        hash = 97 * hash + Objects.hashCode(this.operationId);
        hash = 97 * hash + Objects.hashCode(this.errors);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final BaseResponse other = (BaseResponse) obj;
        if (!Objects.equals(this.auditData, other.auditData)) {
            return false;
        }
        if (!Objects.equals(this.operationId, other.operationId)) {
            return false;
        }
        if (!Objects.equals(this.errors, other.errors)) {
            return false;
        }
        return true;
    }
    
    

}
