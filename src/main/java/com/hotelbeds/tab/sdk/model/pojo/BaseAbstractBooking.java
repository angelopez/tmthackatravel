package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.BookingStatus;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@XmlType(name = "BaseAbstractBooking",
         namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
         propOrder = {"reference","status","currency","pendingAmount","paymentInformation", "transactions"})
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class BaseAbstractBooking {

    
    private String                     reference;

    
    private BookingStatus              status;

    
    private String                     currency;

    
    private BigDecimal                 pendingAmount;

    
    private PaymentInformationResponse paymentInformation;

    
    private List<BookingTransaction>   transactions;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(BigDecimal pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public PaymentInformationResponse getPaymentInformation() {
        return paymentInformation;
    }

    public void setPaymentInformation(PaymentInformationResponse paymentInformation) {
        this.paymentInformation = paymentInformation;
    }

    public List<BookingTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<BookingTransaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.reference);
        hash = 37 * hash + Objects.hashCode(this.status);
        hash = 37 * hash + Objects.hashCode(this.currency);
        hash = 37 * hash + Objects.hashCode(this.pendingAmount);
        hash = 37 * hash + Objects.hashCode(this.paymentInformation);
        hash = 37 * hash + Objects.hashCode(this.transactions);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaseAbstractBooking other = (BaseAbstractBooking) obj;
        if (!Objects.equals(this.reference, other.reference)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        if (!Objects.equals(this.pendingAmount, other.pendingAmount)) {
            return false;
        }
        if (!Objects.equals(this.paymentInformation, other.paymentInformation)) {
            return false;
        }
        if (!Objects.equals(this.transactions, other.transactions)) {
            return false;
        }
        return true;
    }
    
    

}
