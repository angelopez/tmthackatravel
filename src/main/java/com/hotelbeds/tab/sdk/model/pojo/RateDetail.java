package com.hotelbeds.tab.sdk.model.pojo;


import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

/**
 * Created by mcazorla on 3/02/16.
 *
 */
@XmlType(name = "RateDetail")
@XmlAccessorType(XmlAccessType.FIELD)
public class RateDetail {

    
    private String rateKey;

    
    private String retrievePickupsKey;

    
    private String retrieveSeatingKey;

    
    private List<OperationDate> operationDates;

    
    private List<Language> languages;

    
    private List<Session> sessions;
    
    private Duration minimumDuration;

    
    private Duration maximumDuration;

    
    private PaxPrice totalAmount;

    
    private List<PaxPrice> paxAmounts;

    
    private AgencyCommission agencyCommission;

    
    private ActivityPickup pickup;

    public String getRateKey() {
        return rateKey;
    }

    public void setRateKey(String rateKey) {
        this.rateKey = rateKey;
    }

    public String getRetrievePickupsKey() {
        return retrievePickupsKey;
    }

    public void setRetrievePickupsKey(String retrievePickupsKey) {
        this.retrievePickupsKey = retrievePickupsKey;
    }

    public String getRetrieveSeatingKey() {
        return retrieveSeatingKey;
    }

    public void setRetrieveSeatingKey(String retrieveSeatingKey) {
        this.retrieveSeatingKey = retrieveSeatingKey;
    }

    public List<OperationDate> getOperationDates() {
        return operationDates;
    }

    public void setOperationDates(List<OperationDate> operationDates) {
        this.operationDates = operationDates;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }
    public Duration getMinimumDuration() {
        return minimumDuration;
    }

    public void setMinimumDuration(Duration minimumDuration) {
        this.minimumDuration = minimumDuration;
    }

    public Duration getMaximumDuration() {
        return maximumDuration;
    }

    public void setMaximumDuration(Duration maximumDuration) {
        this.maximumDuration = maximumDuration;
    }

    public PaxPrice getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(PaxPrice totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<PaxPrice> getPaxAmounts() {
        return paxAmounts;
    }

    public void setPaxAmounts(List<PaxPrice> paxAmounts) {
        this.paxAmounts = paxAmounts;
    }

    public AgencyCommission getAgencyCommission() {
        return agencyCommission;
    }

    public void setAgencyCommission(AgencyCommission agencyCommission) {
        this.agencyCommission = agencyCommission;
    }

    public ActivityPickup getPickup() {
        return pickup;
    }

    public void setPickup(ActivityPickup pickup) {
        this.pickup = pickup;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.rateKey);
        hash = 89 * hash + Objects.hashCode(this.retrievePickupsKey);
        hash = 89 * hash + Objects.hashCode(this.retrieveSeatingKey);
        hash = 89 * hash + Objects.hashCode(this.operationDates);
        hash = 89 * hash + Objects.hashCode(this.languages);
        hash = 89 * hash + Objects.hashCode(this.sessions);
        hash = 89 * hash + Objects.hashCode(this.minimumDuration);
        hash = 89 * hash + Objects.hashCode(this.maximumDuration);
        hash = 89 * hash + Objects.hashCode(this.totalAmount);
        hash = 89 * hash + Objects.hashCode(this.paxAmounts);
        hash = 89 * hash + Objects.hashCode(this.agencyCommission);
        hash = 89 * hash + Objects.hashCode(this.pickup);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RateDetail other = (RateDetail) obj;
        if (!Objects.equals(this.rateKey, other.rateKey)) {
            return false;
        }
        if (!Objects.equals(this.retrievePickupsKey, other.retrievePickupsKey)) {
            return false;
        }
        if (!Objects.equals(this.retrieveSeatingKey, other.retrieveSeatingKey)) {
            return false;
        }
        if (!Objects.equals(this.operationDates, other.operationDates)) {
            return false;
        }
        if (!Objects.equals(this.languages, other.languages)) {
            return false;
        }
        if (!Objects.equals(this.sessions, other.sessions)) {
            return false;
        }
        if (!Objects.equals(this.minimumDuration, other.minimumDuration)) {
            return false;
        }
        if (!Objects.equals(this.maximumDuration, other.maximumDuration)) {
            return false;
        }
        if (!Objects.equals(this.totalAmount, other.totalAmount)) {
            return false;
        }
        if (!Objects.equals(this.paxAmounts, other.paxAmounts)) {
            return false;
        }
        if (!Objects.equals(this.agencyCommission, other.agencyCommission)) {
            return false;
        }
        if (!Objects.equals(this.pickup, other.pickup)) {
            return false;
        }
        return true;
    }
    
    

}
