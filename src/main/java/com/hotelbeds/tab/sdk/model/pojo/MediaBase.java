package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.MediaMimeTypeAdapter;
import com.hotelbeds.tab.sdk.model.domain.MediaMimeType;
import com.hotelbeds.tab.sdk.model.domain.MediaTagType;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@XmlType(name = "MediaBase")
@XmlAccessorType(XmlAccessType.FIELD)
public class MediaBase implements Comparable<MediaBase> {

    private Integer         visualizationOrder;
    private String          name;
    private String          description;
    //@XmlJavaTypeAdapter(MediaMimeTypeAdapter.class)
    private String mimeType;
    private MediaTagType    tag;
    
    private List<MediaUrl>  urls;
    
    private Duration        duration;

    public MediaBase() {
        super();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MediaTagType getTag() {
        return tag;
    }

    public void setTag(MediaTagType tag) {
        this.tag = tag;
    }

    public Integer getVisualizationOrder() {
        return visualizationOrder;
    }

    public void setVisualizationOrder(Integer visualizationOrder) {
        this.visualizationOrder = visualizationOrder;
    }

    public List<MediaUrl> getUrls() {
        return urls;
    }

    public void setUrls(List<MediaUrl> urls) {
        this.urls = urls;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    @Override
    public int compareTo(MediaBase o) {
        return this.visualizationOrder.compareTo(o.visualizationOrder);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.visualizationOrder);
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.description);
        hash = 29 * hash + Objects.hashCode(this.mimeType);
        hash = 29 * hash + Objects.hashCode(this.tag);
        hash = 29 * hash + Objects.hashCode(this.urls);
        hash = 29 * hash + Objects.hashCode(this.duration);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MediaBase other = (MediaBase) obj;
        if (!Objects.equals(this.visualizationOrder, other.visualizationOrder)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (this.mimeType != other.mimeType) {
            return false;
        }
        if (this.tag != other.tag) {
            return false;
        }
        if (!Objects.equals(this.urls, other.urls)) {
            return false;
        }
        if (!Objects.equals(this.duration, other.duration)) {
            return false;
        }
        return true;
    }
    
    

}
