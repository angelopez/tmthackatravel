package com.hotelbeds.tab.sdk.model.pojo;

import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.*;

@XmlType(name = "LocationPoint",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "type", "meetingPoint", "pickupInstructions"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class LocationPoint {

    
    private String                   type;
    
    private PointOfInterest          meetingPoint;
    
    private List<DuoTypeDescription> pickupInstructions;

    public LocationPoint() {
        super();
    }

    public LocationPoint(String type, PointOfInterest meetingPoint, List<DuoTypeDescription> pickupInstructions) {
        super();
        this.type = type;
        this.meetingPoint = meetingPoint;
        this.pickupInstructions = pickupInstructions;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PointOfInterest getMeetingPoint() {
        return meetingPoint;
    }

    public void setMeetingPoint(PointOfInterest meetingPoint) {
        this.meetingPoint = meetingPoint;
    }

    public List<DuoTypeDescription> getPickupInstructions() {
        return pickupInstructions;
    }

    public void setPickupInstructions(List<DuoTypeDescription> pickupInstructions) {
        this.pickupInstructions = pickupInstructions;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Type=").append(type).append(",");
        sb.append("MeetingPoint=").append(meetingPoint).append(",");
        sb.append("PickupInstructions=").append(pickupInstructions).append(",");

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.type);
        hash = 53 * hash + Objects.hashCode(this.meetingPoint);
        hash = 53 * hash + Objects.hashCode(this.pickupInstructions);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LocationPoint other = (LocationPoint) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.meetingPoint, other.meetingPoint)) {
            return false;
        }
        if (!Objects.equals(this.pickupInstructions, other.pickupInstructions)) {
            return false;
        }
        return true;
    }
    
    

}
