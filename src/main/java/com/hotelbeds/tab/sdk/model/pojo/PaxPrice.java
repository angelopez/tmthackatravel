package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.domain.PaxType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.Objects;

@XmlType(name = "PaxPrice",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "paxType", "ageFrom", "ageTo", "amount","netAmount", "boxOfficeAmount", "mandatoryApplyAmount"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class PaxPrice {

    
    private PaxType     paxType;
    
    private Integer     ageFrom;
    
    private Integer     ageTo;
    
    private BigDecimal  amount;
    
    private BigDecimal  netAmount;
    
    private BigDecimal  boxOfficeAmount;
    
    private Boolean     mandatoryApplyAmount;


    public PaxType getPaxType() {
        return paxType;
    }

    public void setPaxType(PaxType paxType) {
        this.paxType = paxType;
    }

    public Integer getAgeFrom() {
        return ageFrom;
    }

    public void setAgeFrom(Integer ageFrom) {
        this.ageFrom = ageFrom;
    }

    public Integer getAgeTo() {
        return ageTo;
    }

    public void setAgeTo(Integer ageTo) {
        this.ageTo = ageTo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBoxOfficeAmount() {
        return boxOfficeAmount;
    }

    public void setBoxOfficeAmount(BigDecimal boxOfficeAmount) {
        this.boxOfficeAmount = boxOfficeAmount;
    }

    public BigDecimal getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }

    public Boolean getMandatoryApplyAmount() {
        return mandatoryApplyAmount;
    }

    public void setMandatoryApplyAmount(Boolean mandatoryApplyAmount) {
        this.mandatoryApplyAmount = mandatoryApplyAmount;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.paxType);
        hash = 17 * hash + Objects.hashCode(this.ageFrom);
        hash = 17 * hash + Objects.hashCode(this.ageTo);
        hash = 17 * hash + Objects.hashCode(this.amount);
        hash = 17 * hash + Objects.hashCode(this.netAmount);
        hash = 17 * hash + Objects.hashCode(this.boxOfficeAmount);
        hash = 17 * hash + Objects.hashCode(this.mandatoryApplyAmount);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PaxPrice other = (PaxPrice) obj;
        if (this.paxType != other.paxType) {
            return false;
        }
        if (!Objects.equals(this.ageFrom, other.ageFrom)) {
            return false;
        }
        if (!Objects.equals(this.ageTo, other.ageTo)) {
            return false;
        }
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        if (!Objects.equals(this.netAmount, other.netAmount)) {
            return false;
        }
        if (!Objects.equals(this.boxOfficeAmount, other.boxOfficeAmount)) {
            return false;
        }
        if (!Objects.equals(this.mandatoryApplyAmount, other.mandatoryApplyAmount)) {
            return false;
        }
        return true;
    }
    
    
}
