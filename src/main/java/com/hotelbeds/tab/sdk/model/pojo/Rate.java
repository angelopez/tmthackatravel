package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

/**
 * Created by mcazorla on 3/02/16.
 *
 */
@XmlType(name = "Rate",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = { "rateCode", "name", "shortDescription", "paxQuestions", "rateDetails"} )
@XmlAccessorType(XmlAccessType.FIELD)
public class Rate {

    
    private String rateCode;

    
    private String name;

    
    private String shortDescription;

    
    private List<PaxQuestion> paxQuestions;

    
    private List<RateDetail> rateDetails;

    public String getRateCode() {
        return rateCode;
    }

    public void setRateCode(String rateCode) {
        this.rateCode = rateCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public List<PaxQuestion> getPaxQuestions() {
        return paxQuestions;
    }

    public void setPaxQuestions(List<PaxQuestion> paxQuestions) {
        this.paxQuestions = paxQuestions;
    }

    public List<RateDetail> getRateDetails() {
        return rateDetails;
    }

    public void setRateDetails(List<RateDetail> rateDetails) {
        this.rateDetails = rateDetails;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.rateCode);
        hash = 41 * hash + Objects.hashCode(this.name);
        hash = 41 * hash + Objects.hashCode(this.shortDescription);
        hash = 41 * hash + Objects.hashCode(this.paxQuestions);
        hash = 41 * hash + Objects.hashCode(this.rateDetails);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rate other = (Rate) obj;
        if (!Objects.equals(this.rateCode, other.rateCode)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.shortDescription, other.shortDescription)) {
            return false;
        }
        if (!Objects.equals(this.paxQuestions, other.paxQuestions)) {
            return false;
        }
        if (!Objects.equals(this.rateDetails, other.rateDetails)) {
            return false;
        }
        return true;
    }
    
    

}
