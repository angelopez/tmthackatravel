package com.hotelbeds.tab.sdk.model.pojo;

import com.hotelbeds.tab.sdk.model.adapters.DateAdapter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType(name = "PickupOperationDate",
        namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages",
        propOrder = {"from", "to", "maximum", "minimum", "totalAmount", "paxAmounts", "paxQuestions", "cancellationPolicies"})
@XmlAccessorType(XmlAccessType.FIELD)
public class PickupOperationDate {

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date                  from;

    
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date                  to;

    
    private Integer               minimum;

    
    private Integer               maximum;

    
    private PaxPrice              totalAmount;

    
    private List<PaxPrice>        paxAmounts;

    
    private List<PaxQuestion>     paxQuestions;

    
    private List<CancellationPolicy> cancellationPolicies;

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Integer getMinimum() {
        return minimum;
    }

    public void setMinimum(Integer minimum) {
        this.minimum = minimum;
    }

    public Integer getMaximum() {
        return maximum;
    }

    public void setMaximum(Integer maximum) {
        this.maximum = maximum;
    }

    public PaxPrice getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(PaxPrice totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<PaxPrice> getPaxAmounts() {
        return paxAmounts;
    }

    public void setPaxAmounts(List<PaxPrice> paxAmounts) {
        this.paxAmounts = paxAmounts;
    }

    public List<PaxQuestion> getPaxQuestions() {
        return paxQuestions;
    }

    public void setPaxQuestions(List<PaxQuestion> paxQuestions) {
        this.paxQuestions = paxQuestions;
    }

    public List<CancellationPolicy> getCancellationPolicies() {
        return cancellationPolicies;
    }

    public void setCancellationPolicies(List<CancellationPolicy> cancellationPolicies) {
        this.cancellationPolicies = cancellationPolicies;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.from);
        hash = 41 * hash + Objects.hashCode(this.to);
        hash = 41 * hash + Objects.hashCode(this.minimum);
        hash = 41 * hash + Objects.hashCode(this.maximum);
        hash = 41 * hash + Objects.hashCode(this.totalAmount);
        hash = 41 * hash + Objects.hashCode(this.paxAmounts);
        hash = 41 * hash + Objects.hashCode(this.paxQuestions);
        hash = 41 * hash + Objects.hashCode(this.cancellationPolicies);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PickupOperationDate other = (PickupOperationDate) obj;
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.minimum, other.minimum)) {
            return false;
        }
        if (!Objects.equals(this.maximum, other.maximum)) {
            return false;
        }
        if (!Objects.equals(this.totalAmount, other.totalAmount)) {
            return false;
        }
        if (!Objects.equals(this.paxAmounts, other.paxAmounts)) {
            return false;
        }
        if (!Objects.equals(this.paxQuestions, other.paxQuestions)) {
            return false;
        }
        if (!Objects.equals(this.cancellationPolicies, other.cancellationPolicies)) {
            return false;
        }
        return true;
    }

    
    
}
