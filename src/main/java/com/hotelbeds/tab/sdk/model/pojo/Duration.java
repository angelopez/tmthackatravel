package com.hotelbeds.tab.sdk.model.pojo;


import com.hotelbeds.tab.sdk.model.domain.MetricType;
import java.util.Objects;
import javax.xml.bind.annotation.*;

@XmlType(name = "Duration", namespace = "http://www.hotelbeds.com/schemas/actapi/v3/messages")
@XmlAccessorType(XmlAccessType.FIELD)
public class Duration {

    
    private Double value;
    
    private MetricType metric;

    public Duration() {
    }

    public Duration(Double value, MetricType metric) {
        this.value = value;
        this.metric = metric;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public MetricType getMetric() {
        return metric;
    }

    public void setMetric(MetricType metric) {
        this.metric = metric;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("value=" + value);
        sb.append("Metric=" + metric);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.value);
        hash = 37 * hash + Objects.hashCode(this.metric);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Duration other = (Duration) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        if (this.metric != other.metric) {
            return false;
        }
        return true;
    }
    
    
}
