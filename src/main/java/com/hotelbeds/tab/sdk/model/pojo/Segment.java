package com.hotelbeds.tab.sdk.model.pojo;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@XmlType(name = "Segment")
@XmlAccessorType(XmlAccessType.FIELD)
public class Segment {

    
    private Integer          code;
    
    private String           name;
    
    private String           icon;
    
    private List<SubSegment> subSegments;

    public Segment() {
    }

    public Segment(Integer code, String name, String icon, List<SubSegment> subSegments) {
        super();
        this.code = code;
        this.name = name;
        this.icon = icon;
        this.subSegments = subSegments;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<SubSegment> getSubSegments() {
        return subSegments;
    }

    public void setSubSegments(List<SubSegment> subSegments) {
        this.subSegments = subSegments;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Code=").append(code).append(",");
        sb.append("Name=").append(name).append(",");
        sb.append("Icon=").append(icon).append(",");
        sb.append("SubSegments=").append(subSegments);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.code);
        hash = 71 * hash + Objects.hashCode(this.name);
        hash = 71 * hash + Objects.hashCode(this.icon);
        hash = 71 * hash + Objects.hashCode(this.subSegments);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Segment other = (Segment) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.icon, other.icon)) {
            return false;
        }
        if (!Objects.equals(this.subSegments, other.subSegments)) {
            return false;
        }
        return true;
    }
    
    
}